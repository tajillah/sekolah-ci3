// window.addEventListener("blur", function () {
//     console.log(APP_URL);
// });
var app = ((config) => {
	return {
		view_page: (config) => {
			config = $.extend(
				true,
				{
					data: {},
					url: APP_URL,
					page: "profil",
					type: "POST",
					dataType: "json",
					success: function () {},
					complete: function () {},
					error: function () {},
				},
				config
			);

			var ContentAjax = {
				headers: {
					"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
				},
				dataType: "json",
				data: config.data,
				type: config.type,
				url: config.url,
				success: function (data) {
					config.success(data);
				},
				complete: function (response) {
					config.complete(response);
				},
				error: function (error) {
					var err = error.responseJSON;
					config.error(error);
				},
			};
			$.ajax(ContentAjax);
		},
		ajax: (config) => {
			config = $.extend(
				true,
				{
					data: {},
					url: APP_URL,
					type: "POST",
					dataType: "json",
					cache: true,
					contentType: true,
					processData: true,
					message:false,
					success: function () {},
					complete: function () {},
					error: function () {},
				},
				config
			);
			if (config.contentType) {
				config.data.csrf_app = getCookie("csrf_cookie_name");

				var ContentAjax = {
					dataType: "json",
					data: config.data,
					type: config.type,
					url: config.url,
					success: function (data) {
						if (config.message) {
							toastr[data.status](data.message);
						}
						config.success(data);
					},
					complete: function (response) {
						config.complete(response);
					},
					error: function (error) {
						var err = error.responseJSON;
						config.error(error);
					},
				};
			} else {
				config.data.append("csrf_app", getCookie("csrf_cookie_name"));
				var ContentAjax = {
					url: config.url,
					data: config.data,
					type: config.type,
					dataType: config.dataType,
					cache: false,
					contentType: config.contentType,
					processData: config.processData,
					success: function (data) {
						if (config.message) {
							toastr[data.status](data.message);
						}
						config.success(data);
					},
					complete: function (response) {
						config.complete($.parseJSON(response.responseText), response);
					},
					error: function (error) {
						var err = error.responseJSON;
						config.error(error);
					},
				};

				if (config.contentType) {
					if (config.hasOwnProperty("contentType")) {
						ContentAjax["contentType"] = config.contentType;
					}
				}
				if (config.processData) {
					if (config.hasOwnProperty("processData")) {
						ContentAjax["processData"] = config.processData;
					}
				}
			}

			$.ajax(ContentAjax);
		},
		tableAPI: (config) => {
			config = $.extend(
				true,
				{
					el: "maintable",
					el_search: "filter_search",
					searching: true,
					lengthChange: false,
					data: {},
					url: APP_URL,
					type: "POST",
					info: true,
					bLengthChange: true,
					columnDefs: [
						{
							orderable: false,
							targets: 0,
						},
					],
					data_sort: "",
				},
				config
			);

			config.columnDefs.push({
				targets: 0,
				data: config.data_sort,
				orderable: false,
				render: function (data, type, full, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				},
			});

			var tableConfig = {
				searching: config.searching,
				lengthChange: config.lengthChange,
				processing: true,
				serverSide: true,
				destroy: true,
				info: config.info,
				// pageLength: 2,
				bLengthChange: config.bLengthChange,
				dom: "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
				"<'table-responsive'tr>" +
				"<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
				oLanguage: {
					"oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
					"sInfo": "Showing page _PAGE_ of _PAGES_",
					"sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
					"sSearchPlaceholder": "Search...",
					"sLengthMenu": "Results :  _MENU_",
				},
            // "stripeClasses": [],
            // "lengthMenu": [7, 10, 20, 50],
            // "pageLength": 7 
				// oLanguage: {
				//     // "sInfo": "_TOTAL_ show (_START_ to _END_)"
				//     "sInfo": "Total _TOTAL_ (_START_ to _END_)",
				//     "sEmptyTable": "Tidak ada data yang ditampilkan",
				//     "sProcessing": "DataTables is currently busy",
				//     "sInfoEmpty": "No entries to show",
				//     "sZeroRecords": "No records to display"
				// },
				// "columns": [
				//     { "title": "No", "data": "role_id" },
				//     { "title": "Nama", "data": "role_name", "searchable": true },
				//     { "title": "role_id", "data": "role_id" }
				// ],
				ajax: {
					data: function (d) {
						$.extend(
							d,
							$.extend(config.data, {
								csrf_app: getCookie("csrf_cookie_name"),
								menu: config.menu,
								task: config.task,
							})
						);
					},
					url: config.url,
					dataSrc: "data",
					type: config.type,
				},
				columnDefs: config.columnDefs,
			};

			var table = $("#" + config.el).DataTable(tableConfig);
			$(`[data-datatables="${config.el_search}"]`).on("keyup", function () {
				table.search(this.value).draw();
			});
		},
		refresTable: function (el) {
			$("#" + el)
				.DataTable()
				.ajax.reload();
		},
		block: (time = 0) => {
			$(".block-loader").fadeIn();
			if (time > 0) {
				setTimeout(function () {
					$(".block-loader").fadeOut();
				}, time);
			}
		},
		unblock: (time = 100) => {
			setTimeout(function () {
				$(".block-loader").fadeOut();
			}, time);
		},
		selectcombo: (config) => {
			config = $.extend(
				true,
				{
					el: "mySelect",
					data: {},
					url: APP_URL,
					type: "POST",
					dataType: "json",
					cache: true,
					minimumInputLength: 0,
					page: false,
					selectedId: false,
					dropdownParent: $("#showModal"),
				},
				config
			);
			var selectElement = $(config.el);
			var selectedId = config.selectedId; // Ganti dengan ID yang sesuai dari server-side

			if (!config.page) {
				app.ajax({
					url: config.url,
					success: function (res) {
						$.each(res.data, (i, v) => {
							selected = selectedId == v["id"] ? "selected" : "";
							$(config.el).append(
								`<option ${selected} value="${v["id"]}">${v["text"]}</option>`
							);
						});
						setTimeout(() => {
							$(config.el).select2(config);
						}, 400);
					},
				});
				// selectElement.select2({
				//     dropdownParent: $("#showModal"),
				//     ajax: {
				//         url: config.url,
				//         dataType: config.dataType,
				//         processResults: function (data) {
				//             return {
				//                 results: data.results,
				//             };
				//         },
				//     },
				// });
			} else {
				selectElement.select2({
					dropdownParent: $("#showModal"),
					ajax: {
						url: config.url,
						dataType: config.dataType,
						delay: 250,
						data: function (params) {
							return {
								q: params.term,
								page: params.page || 1,
							};
						},
						processResults: function (data, params) {
							params.page = params.page || 1;

							return {
								results: data.results,
								pagination: {
									more: data.pagination.more,
								},
							};
						},
						cache: config.cache,
					},
					minimumInputLength: 0,
				});
			}
		},
		confirmSave:(config)=>{
			config = $.extend(
				true,
				{
					task: null,
					action: null,
					data: {},
					url: APP_URL,
					type: "POST",
					dataType: "json",
					buttonText: "Hapus",
					title: "Hapus Data!!",
					description:
						"Data yang dihapus tidak dapat dikembalikan dan akan menimbulkan efek pada data lain!!",
					icon: "warning",
					page: "delete",
					message : true,
					success: function () {},
					complete: function () {},
					error: function () {},
				},
				config
			);

			Swal.fire({
				title: config.title,
				text: config.description,
				icon: config.icon,
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: config.buttonText,
			}).then((result) => {
				if (result.value) {
					config.data.csrf_app = getCookie("csrf_cookie_name");
					// loadAnimation();
					var ContentAjax = {
						url: config.url,
						data: config.data,
						type: config.type,
						dataType: config.dataType,
						success: function (data) {
							if(config.message){
								if (!data.success) {
									toastr[data.status](data.message);
								} else {
									toastr[data.status](data.message);
								}
							}
							// loadAnimation(100);
							config.success(data);
						},
						complete: function (response) {
							config.complete($.parseJSON(response.responseText), response);
						},
						error: function (error) {
							// var err = error.responseJSON;
							config.error(error);
						},
					};
					$.ajax(ContentAjax);
				}
			});
		}
	};
})();

function go_pages(el) {
	var data = $(el).data();
	app.ajax({
		dataType: "json",
		url: URL + "panel/gopages",
		data: data,
		success: function (res) {
			$("#app-panel-content").html(atob(res["contentHTML"]));
			$("#app-panel-plugins").html(atob(res["plugins"]));
		},
	});
}

function getCookie(name) {
	const value = `; ${document.cookie}`;
	const parts = value.split(`; ${name}=`);
	if (parts.length === 2) return parts.pop().split(";").shift();
}
