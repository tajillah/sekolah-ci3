<?php defined('BASEPATH') or exit('No direct script access allowed');
class Profile extends AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['ProfileModel' => 'Profile']);
    }

    function save()
    {
        $data = request();
        $where['id'] = $this->session->user['biodata_id'];
        $res = $this->Profile->edit($where, $data);
        $this->response($res);
    }

    function password()
    {
        $data = request();
        $save['password'] = password_app($data['password']);
        $where['user_id'] = $this->session->user['user_id'];
        $res = $this->Profile->table('app_shared')->edit($where, $save);
        $this->response($res);
    }

    public function read()
    {
        $where['id'] = $this->session->user['biodata_id'];
        $read = $this->Profile->read($where);
        $this->response($read);
    }

    function avatar()
    {
        $data = request();
        $path = './dokumen/pegawai/avatar/' . $this->session->user['biodata_id'];
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $new_name = $this->session->user['biodata_id'];
        $config['upload_path']          = $path;
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $config['overwrite']           = true;
        $config['file_name']            = $new_name;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_upload_file')) {
            $response['success']    = false;
            $response['status']     = 'error';
            $response['message']    = 'Upload Gagal!!';
            $response['error']      = $this->upload->display_errors();
        } else {
            // $read = $this->db->get_where('v_nilai_asn_tahun', ['id' => $data['id']])->row_array();
            $dataUpload = $this->upload->data();
            $save['avatar']    = $dataUpload['file_name'];;
            $response = $this->Profile->table('pegawai')->edit(['id' => $this->session->user['biodata_id']], $save);
        }
        $this->response($response);
    }
}
