<script>
    $(() => {
        _profile();
    })

    function _profile() {
        app.ajax({
            url: URL + 'profile/profile/read',
            success: function(res) {
                $.each(res, (i, v) => {
                    $(`.lbl-${i}`).html(v);
                    $(`[name="${i}"]`).val(v);
                })
            }
        });
    }

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        app.ajax({
            dataType: 'json',
            url: URL + 'profile/profile/save',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                _profile();
            }
        });
    }

    function changepassword(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        app.ajax({
            dataType: 'json',
            url: URL + 'profile/profile/password',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                $('[name="password"]').val('')
            }
        });
    }

    function goFileUpload() {
        var form = $(`#form-upload`)[0];
        var formData = new FormData(form);
        app.ajax({
            dataType: 'json',
            url: URL + 'profile/profile/avatar',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
            }
        });
    }
</script>