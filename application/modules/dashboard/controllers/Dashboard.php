<?php defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
    }

    public function total(){
        $opt['pegawai'] = $this->db->get('pegawai')->result_array();
        $opt['total_register'] = 0;
        $opt['total_unregister'] = 0;
        foreach ($opt['pegawai'] as $key => $value) {
            if(!empty($value['user_id'])){
                $opt['total_register'] = $opt['total_register']+1;
            }else{
                $opt['total_unregister'] = $opt['total_unregister']+1;
            }
        }
        $this->response($opt);
    }
}
