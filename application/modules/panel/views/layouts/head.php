<!doctype html>
<html lang="en" data-layout="horizontal" data-topbar="light" data-sidebar="dark" data-sidebar-size="sm-hover" data-sidebar-image="none" data-preloader="disable">

<head>

    <meta charset="utf-8" />
    <title><?= $this->config->item('app_title'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url() ?>app-assets/images/favicon.ico">

    <!-- Sweet Alert css-->
    <link href="<?= base_url() ?>app-assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- plugin css -->
    <link href="<?= base_url() ?>app-assets/libs/jsvectormap/css/jsvectormap.min.css" rel="stylesheet" type="text/css" />
    

    <!-- One of the following themes -->
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/libs/@simonwep/pickr/themes/classic.min.css" /> <!-- 'classic' theme -->
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/libs/@simonwep/pickr/themes/monolith.min.css" /> <!-- 'monolith' theme -->
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/libs/@simonwep/pickr/themes/nano.min.css" /> <!-- 'nano' theme -->
    <!-- Layout config Js -->
    <script src="<?= base_url() ?>app-assets/js/layout.js"></script>
    <!-- Bootstrap Css -->
    <link href="<?= base_url() ?>app-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= base_url() ?>app-assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?= base_url() ?>app-assets/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- custom Css-->
    <link href="<?= base_url() ?>app-assets/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>app-assets/libs/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
</head>