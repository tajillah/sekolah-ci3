<ul class="navbar-nav" id="navbar-nav">
    <li class="menu-title"><span data-key="t-menu">Menu</span></li>
    <li class="nav-item">
        <a class="nav-link menu-link" href="#sidebarDashboards" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
            <i class="las la-tachometer-alt"></i> <span data-key="t-dashboards">Dashboards</span>
        </a>
        <div class="collapse menu-dropdown" id="sidebarDashboards">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                    <a href="dashboard-analytics.html" class="nav-link" data-key="t-analytics"> Analytics </a>
                </li>
            </ul>
        </div>
    </li> <!-- end Dashboard Menu -->
    <li class="nav-item">
        <a class="nav-link menu-link" href="#sidebarLayouts">
            <i class="las la-columns"></i> <span data-key="t-layouts">Layouts</span> <span class="badge badge-pill bg-danger" data-key="t-hot">Hot</span>
        </a>
    </li> <!-- end Dashboard Menu -->
</ul>