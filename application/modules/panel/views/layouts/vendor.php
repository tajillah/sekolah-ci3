<!-- JAVASCRIPT -->
<script src="<?= base_url() ?>app-assets/libs/jquery/jquery-3.6.0.min.js"></script>
<script src="<?= base_url() ?>app-assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>app-assets/libs/simplebar/simplebar.min.js"></script>
<script src="<?= base_url() ?>app-assets/libs/node-waves/waves.min.js"></script>
<script src="<?= base_url() ?>app-assets/libs/feather-icons/feather.min.js"></script>
<script src="<?= base_url() ?>app-assets/js/pages/plugins/lord-icon-2.1.0.js"></script>
<script src="<?= base_url() ?>app-assets/js/plugins.js"></script>

<!-- Sweet Alerts js -->
<script src="<?= base_url() ?>app-assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!--datatable js-->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>


<script src="<?= base_url() ?>app-assets/js/pages/file-manager.init.js"></script>
<!-- <script src="assets/js/pages/datatables.init.js"></script> -->
<!-- Vector map-->
<script src="<?= base_url() ?>app-assets/libs/jsvectormap/js/jsvectormap.min.js"></script>
<script src="<?= base_url() ?>app-assets/libs/jsvectormap/maps/world-merc.js"></script>
<!-- App js -->
<script src="<?= base_url() ?>app-assets/libs/toastr/toastr.min.js"></script>

<script src="<?= base_url() ?>app-assets/libs/@simonwep/pickr/pickr.min.js"></script>
<script src="<?= base_url() ?>app-assets/js/pages/form-pickers.init.js"></script>

<!-- App js -->
<!-- <script src="assets/js/app.js"></script> -->

<script src="<?= base_url() ?>app-assets/js/app.js"></script>
<script src="<?= base_url() ?>app-assets/custom/app.js"></script>
<script>
    var URL = '<?= base_url() ?>';
    var APP_URL = '<?= base_url() ?>';
    var USER_ROLE = '<?= strtolower($this->session->user['modul_utama']); ?>';
</script>
<script>
    $(document).ready(function() {
        navMenu();
    });

    function navMenu() {
        app.ajax({
            dataType: 'json',
            url: URL + 'panel/menu',
            success: function(res) {
                $('#navbar-nav').html('');
                $('#navbar-nav').append(res['html']);
                setTimeout(() => {
                    $(`[data-menu="dashboard"]`).click()
                }, 400);
            }
        });
    }
</script>

<div id="app-panel-plugins"></div>