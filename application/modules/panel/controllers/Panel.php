<?php defined('BASEPATH') or exit('No direct script access allowed');

class Panel extends AppController
{
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->user['isLogin'])) {
			redirect(base_url('login'));
		}
		$this->load->model(['MenuModel' => 'Menu']);
	}
	public function index()
	{
		$this->load->view('app');
	}

	public function menu()
	{
		$aArrMenu = $this->Menu->navigation();
		$this->response($aArrMenu);
	}

	function gopages()
	{
		$data = request();
		// $fileDefault = APPPATH . 'modules/panel/views/' . $data['patch'] . '/default';
		// if (!file_exists($fileDefault . '.php')) {
		// 	$filePlugins = APPPATH . 'modules/panel/views/' . $data['patch'] . '/plugins';
		// }

		$data['contentHTML']  	= base64_encode($this->load->view(strtolower($data['patch']) . '/default', [], true));
		$data['plugins']  	= base64_encode($this->load->view(strtolower($data['patch']) . '/plugins', [], true));
		$data['patch'] = strtolower($data['patch']) . '/default';
		$this->response($data);
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
