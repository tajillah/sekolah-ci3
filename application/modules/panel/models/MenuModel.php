<?php
class MenuModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function navigation()
    {
        $html = '';
        $aArrMenu = $this->main_menu();
        foreach ($aArrMenu as $menu) {
            if (empty($menu['sub'])) {
                $html .= '<li class="nav-item">';
                $html .= '    <a class="nav-link menu-link" href="javascript:void(0)" onclick="go_pages(this)" data-app="menu" data-menu="' . $menu['menu_url'] . '" data-patch="' . $menu['menu_patch'] . '" data-title="' . $menu['menu_title'] . '">';
                $html .= '        <i class="' . $menu['menu_icon'] . '"></i> <span>' . ucwords($menu['menu_name']) . '</span>';
                $html .= '    </a>';
                $html .= '</li>';
            } else {
                $subHtml = '';
                foreach ($menu['sub'] as $subMenu) {
                    $subHtml .= '            <li class="nav-item">';
                    $subHtml .= '                <a href="javascript:void(0)" class="nav-link" onclick="go_pages(this)" data-app="menu" data-menu="' . $subMenu['menu_url'] . '" data-patch="' . $subMenu['menu_patch'] . '" data-title="' . $subMenu['menu_title'] . '"> ' . ucwords($subMenu['menu_name']) . ' </a>';
                    $html .= '            </li>';
                    $subHtml .= '    <li>';
                }

                $html .= ' <li class="nav-item">';
                $html .= '    <a class="nav-link menu-link" href="#' . $menu['id'] . '" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="' . $menu['id'] . '">';
                $html .= '        <i class="' . $menu['menu_icon'] . '"></i>  <span>' . ucwords($menu['menu_name']) . '</span>';
                $html .= '    </a>';
                $html .= '    <div class="collapse menu-dropdown" id="' . $menu['id'] . '">';
                $html .= '        <ul class="nav nav-sm flex-column">';
                $html .= $subHtml;
                $html .= '        </ul>';
                $html .= '    </div>';
                $html .= '</li> ';

                // $html .= '<li class="menu single-menu">';
                // $html .= '<a href="#' . $menu['id'] . '" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle autodroprown">';
                // $html .= '    <div class="">';
                // $html .= '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">';
                // $html .= '        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>';
                // $html .= '        <polyline points="9 22 9 12 15 12 15 22"></polyline>';
                // $html .= '    </svg>';
                // $html .= '    <span>Dashboard</span>';
                // $html .= '    </div>';
                // $html .= '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"   stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">';
                // $html .= '    <polyline points="6 9 12 15 18 9"></polyline>';
                // $html .= '    </svg>';
                // $html .= '</a>';
                // $html .= $subHtml;
                // // $html .= '<ul class="collapse submenu list-unstyled" id="' . $menu['id'] . '" data-parent="#topAccordion">';
                // // $html .= '    <li>';
                // // $html .= '    <a href="index.html"> Analytics </a>';
                // // $html .= '    </li>';
                // // $html .= '    <li>';
                // // $html .= '    <a href="index2.html"> Sales </a>';
                // // $html .= '    </li>';
                // // $html .= '</ul>';
                // $html .= '</li>';
            }
        }
        // <li class="menu single-menu">
        //     <a href="#dashboard" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle autodroprown">
        //       <div class="">
        //         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
        //           <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
        //           <polyline points="9 22 9 12 15 12 15 22"></polyline>
        //         </svg>
        //         <span>Dashboard</span>
        //       </div>
        //       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
        //         <polyline points="6 9 12 15 18 9"></polyline>
        //       </svg>
        //     </a>
        //     <ul class="collapse submenu list-unstyled" id="dashboard" data-parent="#topAccordion">
        //       <li>
        //         <a href="index.html"> Analytics </a>
        //       </li>
        //       <li>
        //         <a href="index2.html"> Sales </a>
        //       </li>
        //     </ul>
        //   </li>
        return ['html' => $html];
    }

    function main_menu()
    {
        $user_id =  $this->session->user['user_id'];
        $get_role = $this->db->get_where('app_role_user', ['user_id' => $user_id, 'role_main' => 1])->row_array();
        $aArrMenu = $this->db
            ->order_by('menu_orderBy ASC')
            ->get_where('app_role_menu', [
                'menu_status' => 'enable',
                'role_id' => $get_role['role_id'],
                'menu_parent_id' => null
            ])->result_array();
        $i = 0;
        foreach ($aArrMenu as $p_cat) {
            $aArrMenu[$i]['sub'] = $this->sub_menu($p_cat['id'], $get_role['role_id']);
            $i++;
        }
        return $aArrMenu;
    }

    function sub_menu($id, $role_id)
    {
        $aArrMenu = $this->db
            ->order_by('menu_orderBy ASC')
            ->get_where('app_role_menu', [
                'menu_status' => 'enable',
                'role_id' => $role_id,
                'menu_parent_id' => $id
            ])->result_array();
        $i = 0;
        foreach ($aArrMenu as $p_cat) {
            $aArrMenu[$i]['sub'] = $this->sub_menu($p_cat['id'], $role_id);
            $i++;
        }
        return $aArrMenu;
    }
}
