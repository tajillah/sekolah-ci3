<?php defined('BASEPATH') or exit('No direct script access allowed');
class Tahun extends AppController
{
    protected $table = 'tahun';
    protected $primary_key = 'id';
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['TahunModel' => 'Tahun']);
        $this->load->model(['BulanModel' => 'Bulan']);
    }
    public function maintable()
    {
        $res = $this->datatable
            ->table($this->table)
            ->draw();
        $this->response($res);
    }

    public function show()
    {
        $data = request();
        $read = $this->Tahun->read($data['id']);
        $this->response($read);
    }

    function store()
    {
        $data = request();
        $data['id'] = gen_uuid();
        $data['is_active'] = '0';
        $res = $this->Tahun->store($data);
        if ($res['success']) {
            $monList =  array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember'
            );
            foreach ($monList as $key => $value) {
                $save['id'] = gen_uuid();
                $save['bulan']  = $key;
                $save['nama']   = $value;
                $save['tahun']  = $data['tahun'];
                $save['tahun_id']  = $res['record']['id'];
                $save['hari_kerja']  = 0;
                $this->db->insert('bulan', $save);
            }
        }
        $this->response($res);
    }

    function update()
    {
        $data = request();
        $res = $this->Tahun->edit($data['id'], $data);
        $this->db->update('bulan', ['tahun' => $data['tahun']], ['tahun_id' => $data['id']]);
        $this->response($res);
    }

    function delete()
    {
        $data = request();
        $res = $this->Tahun->destroy($data['id']);
        $this->db->delete('bulan', ['tahun_id' => $data['id']]);
        $this->response($res);
    }
}
