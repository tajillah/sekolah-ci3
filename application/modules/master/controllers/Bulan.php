<?php defined('BASEPATH') or exit('No direct script access allowed');
class Bulan extends AppController
{
    protected $table = 'bulan';
    protected $primary_key = 'id';
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['TahunModel' => 'Tahun']);
        $this->load->model(['BulanModel' => 'Bulan']);
    }
    public function maintable()
    {
        $res = $this->datatable
            ->table($this->table)
            ->draw();
        $this->response($res);
    }

    public function show()
    {
        $data = request();
        $read = $this->Bulan->read($data['id']);
        $this->response($read);
    }

    function update()
    {
        $data = request();
        $res = $this->Bulan->edit($data['id'], $data);
        $this->response($res);
    }
}
