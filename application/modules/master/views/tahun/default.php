<div class="page-title-box d-sm-flex align-items-center justify-content-between">
    <h4 class="mb-sm-0" id="title-pages">Master Tahun</h4>

    <div class="page-title-right">
        <button type="button" onclick="modalform()" class="btn btn-outline-primary mb-2">Tambah</button>
    </div>
</div>
<div class="card">
    <div class="card-body">
    <table id="main-table-data" class="table table-striped table-bordered nowrap table-striped align-middle dataTable">
            <thead class="table-light">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th class="no-content dt-no-sorting">Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


<form id="formData" action="javascript:saveIt('formData')">
    <div id="modalform" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Tahun</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group row mb-4">
                        <label for="tahun" class="col-xl-2 col-sm-3 col-sm-2 col-form-label">Tahun</label>
                        <div class="col-xl-10 col-lg-9 col-sm-10">
                            <input type="text" class="form-control" id="tahun" name="tahun" placeholder="">
                        </div>
                    </div>
                    <!-- <div class="form-group mb-3"> -->
                    <!-- <input type="text" class="form-control" id="tahun" name="tahun" value=""> -->
                    <!-- <small id="emailHelp1" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    <!-- </div> -->
                    <!-- <div class="form-group mb-4"> -->
                    <!-- <input type="text" class="form-control" id="sEmail" aria-describedby="emailHelp1" placeholder="Email address"> -->
                    <!-- </div> -->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>