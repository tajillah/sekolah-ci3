<script>
    var taskAction = '';
    $(() => {
        mainTable()
    })

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        app.ajax({
            dataType: 'json',
            url: URL + 'master/tahun/' + taskAction,
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                if (res['success']) {
                    app.refresTable('main-table-data');
                    $('#modalform').modal('hide');
                }
            }
        });
    }

    function modalform(pID = null) {
        if (pID == null) {
            $('.form-control').val('')
            taskAction = 'store';
            $('#modalform').modal('show');
        } else {
            taskAction = 'update';
            app.ajax({
                dataType: 'json',
                url: URL + 'master/tahun/show',
                data: {
                    'id': pID
                },
                success: function(res) {
                    $.each(res, (i, v) => {
                        $(`[name="${i}"]`).val(v);
                    })
                    $('#modalform').modal('show');
                }
            });
        }
    }

    function mainTable() {
        app.tableAPI({
            el: 'main-table-data',
            url: APP_URL + 'master/tahun/maintable',
            columnDefs: [{
                    targets: 1,
                    orderable: true,
                    data: 'tahun',
                    render: function(data, type, full, meta) {
                        return full['tahun'];
                    }
                },
                {
                    targets: 2,
                    orderable: false,
                    // className: 'd-flex justify-content-end me-4',
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-outline-primary m-2 btn-sm" onclick="modalform('${full['id']}')">Edit</button>
                        <button class="btn btn-outline-danger m-2 btn-sm" onclick="hapusData('${full['id']}')">Delete</button>`;
                    }
                }
            ]
        })
    }

    function hapusData(id) {
        app.confirmSave({
            url: URL + 'master/tahun/delete',
            data: {
                id: id
            },
            success: function(res) {
                app.refresTable('main-table-data');
            }
        });
    }
</script>