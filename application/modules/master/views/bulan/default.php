<div class="d-flex bd-highlight mb-3">
    <div class="mr-auto bd-highlight">
        <h4>Master Bulan</h4>
    </div>
    <div class="bd-highlight">
        <!-- <button type="button" onclick="modalform()" class="btn btn-outline-primary mb-2">Tambah</button> -->
    </div>
    <!-- <div class="bd-highlight">Flex item</div> -->
</div>
<div class="card">
    <div class="card-body py-2">
        <table id="main-table-data" class="table table-striped table-bordered nowrap table-striped align-middle dataTable">
            <thead class="table-light">
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Tahun</th>
                    <th>Hari Kerja</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


<form id="formData" action="javascript:saveIt('formData')">
    <div id="modalform" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Hari Kerja</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group row mb-4">
                        <label for="hari_kerja" class="col-xl-4 col-sm-3 col-sm-4 col-form-label">Hari Kerja</label>
                        <div class="col-xl-8 col-lg-9 col-sm-8">
                            <input type="number" class="form-control form-control-sm" id="hari_kerja" name="hari_kerja" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>