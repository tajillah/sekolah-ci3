<?php defined('BASEPATH') or exit('No direct script access allowed');
class App extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
    }

    public function read(){
        $aArrData = $this->db->get('app_config')->result_array();
        $this->response($aArrData);
    }

    function save(){
        $data =  request();
        foreach ($data as $key => $value) {
            $record[$key] = $value;
            $save['app_value'] = $value;
            $where['app_kode'] = $key;
            $res =  $this->db->update('app_config',$save,$where);
        }
        $response['success']    = $res;
        $response['status']     = $res;
        $response['status']     = $res ? 'success' : 'error';
        $response['message']    = $res ? 'Berhasil Memperbarui Data!!' : 'Gagal Memperbarui Data!!';
        $this->response($response);
    }
}
