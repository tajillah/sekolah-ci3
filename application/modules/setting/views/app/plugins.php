<script>
    $(() => {
        setting()
    })

    function setting() {
        app.ajax({
            dataType: 'json',
            url: URL + 'setting/app/read',
            success: function(res) {
                $.each(res, (i, v) => {
                    $(`[name="${v.app_kode}"]`).val(v.app_value).trigger('change')
                })
            }
        });
    }

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        app.ajax({
            url: URL + 'setting/app/save',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                console.log(res)
            }
        });
    }
</script>