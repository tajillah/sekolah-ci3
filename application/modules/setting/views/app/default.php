<div class="row">
    <div class="col-md-6 layout-spacing layout-top-spacing">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <h4>Setting</h4>
                </div>
            </div>
            <div class="card-body">
                <form id="formSetting" action="javascript:saveIt('formSetting')" method="post">
                    <div class="form-group mb-3">
                        <label class="form-label required" for="app_title">Nama Aplikasi</label>
                        <input type="text" required class="form-control" id="app_title" name="app_title" value="">
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label required" for="app_desk">Deskripsi App</label>
                        <textarea required class="form-control" id="app_desk" name="app_desk" cols="30" rows="3"></textarea>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label required" for="app_keyword">Deskripsi App</label>
                        <textarea required class="form-control" id="app_keyword" name="app_keyword" cols="30" rows="3"></textarea>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label required" for="app_url">URL Apliksai</label>
                        <input type="text" required class="form-control" id="app_url" name="app_url" value="">
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label required" for="app_skl_nm">Nama Sekolah</label>
                        <input type="text" required class="form-control" id="app_skl_nm" name="app_skl_nm" value="">
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label required" for="app_skl_almt">Alamat Sekolah</label>
                        <textarea required class="form-control" id="app_skl_almt" name="app_skl_almt" cols="30" rows="5"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>