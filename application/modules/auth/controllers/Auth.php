<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!empty($this->session->user['isLogin'])) {
			redirect(base_url('panel'));
		}
	}
	public function index()
	{
		if (!empty($this->session->user['isLogin'])) {
			if ($this->session->user['isLogin']) {
				redirect(base_url('main'));
			} else {
				$this->load->view('default');
			}
		} else {
			$this->load->view('default');
		}
	}

	public function do_login()
	{
		$sUsername = request('username');
		$sPassword = request('password');
		$where['email'] = $sUsername;
		$where['password'] = password_app($sPassword);
		$user =  $this->db->get_where('app_shared', $where)->row_array();
		// $this->response($user);
		// $this->response(request());

		if (!empty($user)) {
			// 	// $biodata_id = $this->create_biodata($user);
			$ses_user = [
				'user_id'       => $user['user_id'],
				'biodata_id'    => $user['biodata_id'],
				// 'biodata_id'    => $biodata_id,
				'username'      => $user['username'],
				'nm_lkp_user'	=> $user['user_full_name'],
				'url'           => base_url('panel'),
				'modul_utama'   => $user['user_role_url'],
				'modul_url'   	=> $user['user_role_url'],
				'aArrModul'		=> $this->db->select('role_url,role_name,role_icon')->get_where('app_role_user', ['user_id' => $user['user_id']])->result_array(),
				'isLogin'       => true
			];

			$app['module_active'] = $user['user_role_url'];


			$this->session->set_userdata([
				'user'  => $ses_user,
				'app'   => $app,
			]);
			$ses_user['success'] = true;
			$this->response($ses_user);
		}else{
			$ses_user['success'] = false;
			$this->response($ses_user);
		}
		// $this->response($this->input->post());
		// echo json_encode(request());
	}
}
