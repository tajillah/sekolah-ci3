<?php defined('BASEPATH') or exit('No direct script access allowed');
class Bukti_kegiatan extends AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['KehadiranGuruModel' => 'KehadiranGuru']);
    }

    public function maintable()
    {
        $data = request();
        $where = [];
        $where['peg_id'] = null;
        if (!empty($data['id'])) {
            $where['peg_id'] = $data['id'];
        }
        // if (strtolower($this->session->user['modul_utama']) != 'admin') {
        //     $where['peg_id'] = $this->session->user['biodata_id'];;
        // }
        // $where['bulan'] = $data['bulan'];
        // $where['tahun'] = $data['tahun'];
        $res = $this->datatable
            ->table('bukti_kinerja_guru')
            ->where($where)
            ->draw();
        $this->response($res);
    }

    function listpeg()
    {
        $data = request();
        if (!empty($data['name'])) {
            $this->db->like("nm_lkp_peg", $data['name']);
        }
        $opr['pegawai'] = $this->db->get('pegawai')->result_array();
        // $opr['ss'] = $this->db->last_query();
        if (!empty($data['name'])) {
            $this->db->like('nm_lkp_peg', "%{$data['name']}%");
            $opr['ss'] = $this->db->last_query();
        }

        $opr['listkegiatan'] = $this->db->get('kegiatan_tipe')->result_array();
        $this->response($opr);
    }

    function save()
    {
        $data = request();
        $data['tanggal'] = date('Y-m-d', strtotime($data['tanggal']));
        $read['tahun'] =  date('Y', strtotime($data['tanggal']));
        $read['bulan'] =  date('m', strtotime($data['tanggal']));
        if (!is_dir('./dokumen/bukti/kegiatan/' . $read['tahun'] . '-' . $read['bulan'])) {
            mkdir('./dokumen/bukti/kegiatan/' . $read['tahun'] . '-' . $read['bulan'], 0777, true);
        }

        $formType = 'kehadiran-' . $read['bulan'] . $read['tahun'];
        // $new_name = time().$_FILES["userfiles"]['name'];
        $new_name = $formType . time();
        $config['upload_path']          = './dokumen/bukti/kegiatan/' . $read['tahun'] . '-' . $read['bulan'] . '/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $config['file_name']            = $new_name;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_upload_file')) {
            $response['success']    = false;
            $response['status']     = 'error';
            $response['message']    = 'Upload Gagal!!';
            $response['error']      = $this->upload->display_errors();
        } else {
            // $read = $this->db->get_where('v_nilai_asn_tahun', ['id' => $data['id']])->row_array();
            $dataUpload = $this->upload->data();
            $save['tanggal']    = $data['tanggal'];
            $save['tipe_bukti'] = $data['tipe_bukti'];
            $save['file_patch'] = $config['upload_path'];
            $save['id_bukti']   = gen_uuid();
            $save['bukti_nama'] = $dataUpload['raw_name'];
            $save['bukti_file_name'] = $dataUpload['file_name'];
            $save['bukti_nama_original'] = $dataUpload['client_name'];
            $save['tgl_upload'] = date('Y-m-d H:i:s');
            // $save['parent_id']  = $data['id'];
            $save['peg_id']     = $data['peg_id'];
            $save['upload_by']  =  $this->session->user['biodata_id'];
            $save['file_ext']   = $dataUpload['file_ext'];
            $save['image_type'] = $dataUpload['image_type'];
            $save['bulan'] = $read['bulan'];
            $save['tahun'] = $read['tahun'];

            $response = $this->KehadiranGuru->table('bukti_kinerja_guru')->store($save);
        }
        $this->response($response);
    }

    function deletefile()
    {
        $data = request();
        $where['id_bukti'] = $data['id'];

        $getbukti = $this->db->get_where('bukti_kinerja_guru', $where)->row_array();

        if (unlink($getbukti['file_patch'] . $getbukti['bukti_file_name'])) {
            $response = $this->KehadiranGuru->table('bukti_kinerja_guru')->destroy($where);
        } else {
            $response['success']    = false;
            $response['status']     = 'error';
            $response['message']    = 'Gagal menghapus file!!';
        }

        $this->response($response);
    }
}
