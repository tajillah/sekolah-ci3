<?php defined('BASEPATH') or exit('No direct script access allowed');
class Tahun extends AppController
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model(['master/TahunModel' => 'Tahun']);
    }

    function list(){
        $data['tahun'] = $this->db->order_by('tahun DESC')->get('tahun')->result_array();
        $data['bulan'] = $this->db->order_by('bulan ASC')->get_where('bulan',['tahun' => $data['tahun'][0]['tahun']])->result_array();
        $this->response($data);
    }
}
