<?php defined('BASEPATH') or exit('No direct script access allowed');
class KehadiranGuru extends AppController
{
    protected $table = 'pegawai';
    protected $primary_key = 'id';
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['KehadiranGuruModel' => 'KehadiranGuru']);
        // $this->load->model(['master/TahunModel' => 'Tahun']);
    }

    public function maintable()
    {
        $data = request();
        $this->KehadiranGuru->generate();
        if (strtolower($this->session->user['modul_utama']) != 'admin') {
            $where['peg_id'] = $this->session->user['biodata_id'];;
        }
        $where['bulan'] = $data['bulan'];
        $where['tahun'] = $data['tahun'];
        $res = $this->datatable
            ->table('v_rekap_kehadiran')
            ->where($where)
            ->draw();
        $res['ssss'] = $where;
        $this->response($res);
    }

    public function show()
    {
        $data = request();
        $read = $this->db->get_where('v_rekap_kehadiran', ['id' => $data['id']])->row_array();
        $read['data_file'] = $this->db->get_where('bukti_kinerja_guru', ['parent_id' => $data['id'], 'tipe_bukti' => $data['taskUpload']])->result_array();
        $read['total_file']  = count($read['data_file']);
        $this->response($read);
    }

    function save()
    {
        $data = request();
        // $update['kehadiran'] = $data['kehadiran'];
        $opr = $this->KehadiranGuru->table('kehadiran_rekap')->edit($data['id'], $data);
        $this->response($opr);
    }

    function upload()
    {
        $data = request();
        $read = $this->db->get_where('v_rekap_kehadiran', ['id' => $data['id']])->row_array();
        if (!is_dir('./dokumen/bukti/'.$data['taskUpload'].'/' . $read['tahun'] . '-' . $read['bulan'])) {
            mkdir('./dokumen/bukti/'.$data['taskUpload'].'/' . $read['tahun'] . '-' . $read['bulan'], 0777, true);
        }

        $formType = 'kehadiran-' . $read['bulan'] . $read['tahun'];
        // $new_name = time().$_FILES["userfiles"]['name'];
        $new_name = $formType . time();
        $config['upload_path']          = './dokumen/bukti/'.$data['taskUpload'].'/' . $read['tahun'] . '-' . $read['bulan'] . '/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $config['file_name']            = $new_name;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_upload_'.$data['taskUpload'])) {
            $response['success']    = false;
            $response['status']     = 'error';
            $response['message']    = 'Upload Gagal!!';
            $response['error']      = $this->upload->display_errors();
        } else {
            // $read = $this->db->get_where('v_nilai_asn_tahun', ['id' => $data['id']])->row_array();
            $dataUpload = $this->upload->data();
            $save['tipe_bukti'] = $data['taskUpload'];
            $save['file_patch'] = $config['upload_path'];
            $save['id_bukti']   = gen_uuid();
            $save['bukti_nama'] = $dataUpload['raw_name'];
            $save['bukti_file_name'] = $dataUpload['file_name'];
            $save['bukti_nama_original'] = $dataUpload['client_name'];
            $save['tgl_upload'] = date('Y-m-d H:i:s');
            $save['parent_id']  = $data['id'];
            $save['peg_id']     = $read['peg_id'];
            $save['upload_by']  =  $this->session->user['biodata_id'];
            $save['file_ext']   = $dataUpload['file_ext'];
            $save['image_type'] = $dataUpload['image_type'];

            $response = $this->KehadiranGuru->table('bukti_kinerja_guru')->store($save);
            $response['data_file'] = $this->db->get_where('bukti_kinerja_guru', ['parent_id' => $data['id'], 'tipe_bukti' => 'kehadiran'])->result_array();
            $response['total_file']  = count($response['data_file']);
        }
        $this->response($response);
    }

    function deletefile()
    {
        $data = request();
        $where['id_bukti'] = $data['id'];

        $getbukti = $this->db->get_where('bukti_kinerja_guru', $where)->row_array();

        if (unlink($getbukti['file_patch'] . $getbukti['bukti_file_name'])) {
            $response = $this->KehadiranGuru->table('bukti_kinerja_guru')->destroy($where);
            $response['data_file'] = $this->db->get_where('bukti_kinerja_guru', ['parent_id' => $data['parent_id'], 'tipe_bukti' => $getbukti['tipe_bukti']])->result_array();
            $response['total_file']  = count($response['data_file']);
        } else {
            $response['success']    = false;
            $response['status']     = 'error';
            $response['message']    = 'Gagal menghapus file!!';
        }

        $this->response($response);
    }
}
