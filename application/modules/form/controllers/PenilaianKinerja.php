<?php defined('BASEPATH') or exit('No direct script access allowed');
class PenilaianKinerja extends AppController
{
    protected $table = 'pegawai';
    protected $primary_key = 'id';
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['PenilaianKinerjaModel' => 'PenilaianKinerja']);
        // $this->load->model(['master/TahunModel' => 'Tahun']);
    }

    public function maintable()
    {
        $data = request();
        $this->PenilaianKinerja->generate();
        if (strtolower($this->session->user['modul_utama']) != 'admin') {
            $where['peg_id'] = $this->session->user['biodata_id'];;
        }
        $where['tahun'] = $data['tahun'];

        $res = $this->datatable
            ->table('v_nilai_asn_tahun')
            ->where($where)
            ->draw();
        $this->response($res);
    }

    public function show()
    {
        $data = request();
        $read = $this->db->get_where('v_nilai_asn_tahun', ['id' => $data['id']])->row_array();
        $read['data_file'] = $this->db->get_where('bukti_kinerja_guru', ['parent_id' => $data['id'], 'tipe_bukti' => $data['tipe']])->result_array();
        $read['total_file']  = count($read['data_file']);
        $this->response($read);
    }

    function save()
    {
        $data = request();
        $bSave = true;
        if (!empty($data['nilai_perangkat'])) {
            // =IF(Q5<=2,5;"SB";IF(Q5<=5;"B";IF(Q5<=7,5;"C";IF(Q5<=10;"K"))))
            $nilai = (float)$data['nilai_perangkat'];
            if ($nilai >= 91) {
                $data['nilai_perangkat_predikat']  = 'SB';
            } else if ($nilai >= 81 && $nilai <= 90) {
                $data['nilai_perangkat_predikat']  = 'B';
            } else if ($nilai >= 71 && $nilai <= 80) {
                $data['nilai_perangkat_predikat']  = 'C';
            } else {
                $data['nilai_perangkat_predikat']  = 'K';
            }
        }
        if (!empty($data['nilai_pengumpulan'])) {
            // =IF(Q5<=2,5;"SB";IF(Q5<=5;"B";IF(Q5<=7,5;"C";IF(Q5<=10;"K"))))
            $nilai = (float)$data['nilai_pengumpulan'];
            if ($nilai >= 91) {
                $data['nilai_pengumpulan_predikat']  = 'SB';
            } else if ($nilai >= 81 && $nilai <= 90) {
                $data['nilai_pengumpulan_predikat']  = 'B';
            } else if ($nilai >= 71 && $nilai <= 80) {
                $data['nilai_pengumpulan_predikat']  = 'C';
            } else {
                $data['nilai_pengumpulan_predikat']  = 'K';
            }
        }
        // $update['nilai_perangkat'] = $data['nilai_perangkat'];
        // $update['nilai_pengumpulan'] = $data['nilai_pengumpulan'];
        // // $update['nilai_perangkat_predikat'] = $data['nilai_perangkat_predikat'];
        // // $update['nilai_pengumpulan_predikat'] = $data['nilai_pengumpulan_predikat'];
        // $update['note_skp_bulanan'] = $data['note_skp_bulanan'];
        // $update['note_bukti_karya_ilmiah'] = $data['note_bukti_karya_ilmiah'];
        // $update['nilai_predikat_guru'] = $data['nilai_predikat_guru'];
        $opr = $this->PenilaianKinerja->table('nilai_asn_tahun')->edit($data['id'], $data);
        $this->response($opr);
    }

    function upload()
    {
        $data = request();
        $formType = $data['tipe'];
        // $new_name = time().$_FILES["userfiles"]['name'];
        $new_name = $formType . time();
        if (!is_dir('./dokumen/bukti/' . $data['tipe'] . '/')) {
            mkdir('./dokumen/bukti/' . $data['tipe'] . '/', 0777, true);
        }
        $config['upload_path']          = './dokumen/bukti/' . $data['tipe'] . '/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $config['file_name']            = $new_name;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_upload_bukti')) {
            $response['success']    = false;
            $response['status']     = 'error';
            $response['message']    = 'Upload Gagal!!';
            $response['error']      = $this->upload->display_errors();
        } else {
            $read = $this->db->get_where('v_nilai_asn_tahun', ['id' => $data['id']])->row_array();
            $dataUpload = $this->upload->data();
            $save['tipe_bukti'] = $data['tipe'];
            $save['file_patch'] = $config['upload_path'];
            $save['id_bukti']   = gen_uuid();
            $save['bukti_nama'] = $dataUpload['raw_name'];
            $save['bukti_file_name'] = $dataUpload['file_name'];
            $save['bukti_nama_original'] = $dataUpload['client_name'];
            $save['tgl_upload'] = date('Y-m-d H:i:s');
            $save['parent_id']  = $data['id'];
            $save['peg_id']     = $read['peg_id'];
            $save['upload_by']  =  $this->session->user['biodata_id'];
            $save['file_ext']   = $dataUpload['file_ext'];
            $save['image_type'] = $dataUpload['image_type'];

            $response = $this->PenilaianKinerja->table('bukti_kinerja_guru')->store($save);
            $response['data_file'] = $this->db->get_where('bukti_kinerja_guru', ['parent_id' => $data['id'], 'tipe_bukti' => $data['tipe']])->result_array();
            $response['total_file']  = count($response['data_file']);
            switch ($data['tipe']) {
                case 'prestasi_siswa':
                    $update['note_bukti_prestasi_siswa'] = count($response['data_file']) > 0 ? 'Ada' : 'Tidak Ada';
                    $this->db->update('nilai_asn_tahun', $update, ['id' => $data['id']]);
                    break;
                default:
                    break;
            }
        }
        $this->response($response);
    }

    function deletefile()
    {
        $data = request();
        $where['id_bukti'] = $data['id'];

        $getbukti = $this->db->get_where('bukti_kinerja_guru', $where)->row_array();

        if (unlink($getbukti['file_patch'] . $getbukti['bukti_file_name'])) {
            $response = $this->PenilaianKinerja->table('bukti_kinerja_guru')->destroy($where);
            $response['data_file'] = $this->db->get_where('bukti_kinerja_guru', ['parent_id' => $data['parent_id'], 'tipe_bukti' => $getbukti['tipe_bukti']])->result_array();
            $response['total_file']  = count($response['data_file']);
        } else {
            $response['success']    = false;
            $response['status']     = 'error';
            $response['message']    = 'Gagal menghapus file!!';
        }

        $this->response($response);
    }

    function bukti()
    {
        $data = request();
        $read = $this->db->get_where('v_nilai_asn_tahun', ['id' => $data['id']])->row_array();
        $read['data_file'] = $this->db->get_where('bukti_kinerja_guru', ['parent_id' => $data['id'], 'tipe_bukti' => $data['tipe']])->result_array();
        $read['total_file']  = count($read['data_file']);
    }
}
