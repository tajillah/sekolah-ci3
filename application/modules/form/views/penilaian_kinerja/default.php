<div class="d-flex bd-highlight mb-3">
    <div class="mr-auto bd-highlight">
        <h4>Data Penilaian Kinerja</h4>
    </div>
    <div class="bd-highlight">
        <!-- <button type="button" onclick="modalform()" class="btn btn-outline-primary mb-2">Tambah</button> -->
    </div>
    <!-- <div class="bd-highlight">Flex item</div> -->
</div>

<div class="card mb-4">
    <div class="card-body">
        <form action="javascript:onFilterPegawai()" autocomplete="off" id="form_filter_tamu" method="post">
            <div class="row">
                <div class="col-12 col-md">
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select id="filter_tahun" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <!-- <div class="form-group">
                                <label>Bulan</label>
                                <select id="filter_bulan" class="form-control"></select>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-2 align-self-center">
                    <button class="btn btn-sm btn-outline-primary mb-3 w-100" type="submit"><i class="fa fa-search"></i> Cari</button>
                    <!-- <button type="button" class="btn btn-sm btn-outline-secondary w-100" onclick="onFilterTamu(true)"><i class="fa fa-times"></i> Reset</button> -->
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <table id="main-table-data" class="table table-striped table-bordered nowrap table-striped align-middle dataTable">
            <thead class="table-light">
                <tr>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">NIP</th>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">NAMA</th>
                    <th colspan="2" style="vertical-align: middle;text-align: center;">Nilai</th>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">Upload Bukti</th>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">Hasil Supervisi</th>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">Predikat ASN</th>
                    <th rowspan="2" style="vertical-align: middle;text-align: center;">Actions</th>
                </tr>
                <tr>
                    <th style="text-align: center;">Perangkat</th>
                    <th style="text-align: center;">Pengumpulan</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


<form id="form-supervisi" action="javascript:saveIt('form-supervisi')">
    <div id="modal-supervisi" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Supervisi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">NIP</label>
                        <input type="text" readonly class="form-control" id="nip" name="nip" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="nm_lkp_peg" class="col-form-label">NAMA LENGKAP</label>
                        <input type="text" readonly class="form-control" required id="nm_lkp_peg" name="nm_lkp_peg" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label class="col-form-label">Supervisi Sekolah</label>
                        <textarea class="form-control" id="note_supervisi_sekolah" name="note_supervisi_sekolah" cols="10" rows="5"></textarea>
                    </div>
                    

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary hidden-form">Save</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="form-bukti" action="javascript:saveIt('form-bukti')">
    <div id="modal-bukti" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Bukti <span class="title-bukti"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">NIP</label>
                        <input type="text" readonly class="form-control" id="nip" name="nip" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="nm_lkp_peg" class="col-form-label">NAMA LENGKAP</label>
                        <input type="text" readonly class="form-control" required id="nm_lkp_peg" name="nm_lkp_peg" placeholder="">
                    </div>
                    <div class="form-group hidden-form">
                        <label class="col-form-label">Upload File</label>
                        <div class="row">
                            <div class="col-md-10">
                                <input type="file" class="form-control" name="file_upload_bukti" accept="image/*">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-outline-primary mb-2" onclick="fileUploadbukti('form-bukti')"><i class="las la-save"></i></button>
                            </div>
                        </div>
                    </div>
                    <div id="list-file-bukti"></div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary hidden-form">Save</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="form-pengumpulan-nilai" action="javascript:saveIt('form-pengumpulan-nilai')">
    <div id="modal-pengumpulan-nilai" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Penilaian <span class="title-modal"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">NIP</label>
                        <input type="text" readonly class="form-control" id="nip" name="nip" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="nm_lkp_peg" class="col-form-label">NAMA LENGKAP</label>
                        <input type="text" readonly class="form-control" required id="nm_lkp_peg" name="nm_lkp_peg" placeholder="">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Pengumpulan Perangkat</label>
                        <input type="number" class="form-control form-readonly" required id="nilai_pengumpulan" name="nilai_pengumpulan" placeholder="">
                    </div>
                    <div class="form-group hidden-form">
                        <label class="col-form-label">Upload Bukti Prangkat</label>
                        <div class="row">
                            <div class="col-md-10">
                                <input type="file" class="form-control" name="file_upload_bukti" accept="image/*">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-outline-primary mb-2" onclick="fileUpload('form-pengumpulan-nilai')"><i class="las la-save"></i></button>
                            </div>
                        </div>
                    </div>
                    <div id="list-file-pengumpulan"></div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary hidden-form">Save</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="form-prangkat" action="javascript:saveIt('form-prangkat')">
    <div id="modal-prangkat" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Penilaian <span class="title-modal"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">NIP</label>
                        <input type="text" readonly class="form-control" id="nip" name="nip" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="nm_lkp_peg" class="col-form-label">NAMA LENGKAP</label>
                        <input type="text" readonly class="form-control" required id="nm_lkp_peg" name="nm_lkp_peg" placeholder="">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Pengumpulan Perangkat</label>
                        <input type="number" class="form-control form-readonly" required id="nilai_perangkat" name="nilai_perangkat" placeholder="">
                    </div>
                    <div class="form-group hidden-form">
                        <label class="col-form-label">Upload Bukti Prangkat</label>
                        <div class="row">
                            <div class="col-md-10">
                                <input type="file" class="form-control" name="file_upload_bukti" accept="image/*">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-outline-primary mb-2" onclick="fileUpload('form-prangkat')"><i class="las la-save"></i></button>
                            </div>
                        </div>
                    </div>
                    <div id="list-file-perangkat"></div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary hidden-form">Save</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>