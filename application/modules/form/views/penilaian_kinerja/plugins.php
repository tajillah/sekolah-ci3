<script>
    var taskAction = '';
    var modalShow = '';
    var selected_tahun = '01';
    var selected_bulan = '01';
    var title = '';
    var tahun = '';

    $(() => {
        if (USER_ROLE != 'admin') {
            $('.hidden-form').hide();
            $('.form-readonly').attr('readonly', true);
        }
        comboYear()
    })

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        formData.append('tahun', selected_tahun);
        formData.append('bulan', selected_bulan);
        app.ajax({
            dataType: 'json',
            url: URL + 'form/PenilaianKinerja/save',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                if (res['success']) {
                    app.refresTable('main-table-data');
                    $('#' + modalShow).modal('hide');
                }
            }
        });
    }

    function fileUpload(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        formData.append('tipe', taskAction);
        app.ajax({
            dataType: 'json',
            url: URL + 'form/PenilaianKinerja/upload',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                $('#list-file-' + taskAction).html('');
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-' + taskAction).append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)
                    })
                }
            }
        });
    }

    function fileUploadbukti(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        formData.append('tipe', taskAction);
        app.ajax({
            dataType: 'json',
            url: URL + 'form/PenilaianKinerja/upload',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                $('#list-file-bukti').html('');
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-bukti').append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)
                    })
                }
            }
        });
    }

    function modalNilai(pID = null, modalName = '', title_modal, target) {
        taskAction = target;
        $('.title-modal').html(title + ' ' + title_modal + `[${tahun}]`);
        $('#list-file-' + taskAction).html('');
        modalShow = modalName;
        app.ajax({
            dataType: 'json',
            url: URL + 'form/PenilaianKinerja/show',
            data: {
                'id': pID,
                'tipe': taskAction
            },
            success: function(res) {
                $.each(res, (i, v) => {
                    $(`[name="${i}"]`).val(v);
                })
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-' + taskAction).append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)
                    })
                }
                $('#' + modalName).modal('show');
            }
        });
    }

    function delFile(id, parent_id) {
        app.confirmSave({
            url: URL + 'form/PenilaianKinerja/deletefile',
            data: {
                id: id,
                parent_id: parent_id
            },
            success: function(res) {
                $('#list-file-' + taskAction).html('');
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-' + taskAction).append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)

                    })
                }

                $('#list-file-bukti').html('');
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-bukti').append(`
                        <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)

                    })
                }
            }
        });
    }

    function modalBukti(pID, target, title_modal) {
        $('#list-file-bukti').html('');
        taskAction = target;
        if (USER_ROLE != 'admin') {
            $('.hidden-form').hide();
            $('.form-readonly').attr('readonly', true);
        }
        app.ajax({
            dataType: 'json',
            url: URL + 'form/PenilaianKinerja/show',
            data: {
                'id': pID,
                'tipe': target
            },
            success: function(res) {
                $.each(res, (i, v) => {
                    $(`[name="${i}"]`).val(v);
                })
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-bukti').append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)
                    })
                }

                $('.title-bukti').html(title_modal + `[${tahun}]`);
                $('#modal-bukti').modal('show');
            }
        });

    }

    function mainTable() {
        app.tableAPI({
            el: 'main-table-data',
            url: APP_URL + 'form/PenilaianKinerja/maintable',
            data: {
                tahun: selected_tahun
            },
            columnDefs: [{
                    targets: 0,
                    orderable: true,
                    data: 'nip',
                    render: function(data, type, full, meta) {
                        return full['nip'];
                    }
                },
                {
                    targets: 1,
                    orderable: true,
                    data: 'nm_lkp_peg',
                    render: function(data, type, full, meta) {
                        return full['nm_lkp_peg'];
                    }
                },
                {
                    targets: 2,
                    orderable: true,
                    data: 'nilai_perangkat',
                    // className: 'text-center',
                    render: function(data, type, full, meta) {
                        return `
                            <span class="mb-4"> Nilai : <span class="mb-2"> ${full['nilai_perangkat']} </span> </span><br> 
                            <span class="mb-4"> Predikat : <span class="mb-2"> ${full['nilai_perangkat_predikat']} </span> </span><br> 
                        `;
                    }
                },
                {
                    targets: 3,
                    orderable: true,
                    data: 'nilai_pengumpulan',
                    // className: 'text-center',
                    render: function(data, type, full, meta) {
                        return `
                            <span class="mb-4"> Nilai : <span class="mb-2"> ${full['nilai_pengumpulan']} </span> </span><br> 
                            <span class="mb-4"> Predikat : <span class="mb-2"> ${full['nilai_pengumpulan_predikat']} </span> </span><br> 
                        `;
                    }
                },
                {
                    targets: 4,
                    orderable: true,
                    data: 'note_bukti_prestasi_siswa',
                    // className: 'text-center',
                    render: function(data, type, full, meta) {
                        prestasi = full['note_bukti_prestasi_siswa'] == 'Tidak ada' ? 'badge badge-soft-warning' : 'badge badge-soft-success';
                        skp = full['note_skp_bulanan'] == 'Tidak Sesuai deadline' ? 'badge badge-soft-warning' : 'badge badge-soft-success';
                        karya = full['note_bukti_karya_ilmiah'] == 'Tidak ada' ? 'badge badge-soft-warning' : 'badge badge-soft-success';
                        return `
                            <span class="mb-4"> Prestasi Siswa : <span class="${prestasi} mb-2"> ${full['note_bukti_prestasi_siswa']} </span> </span><br> 
                            <span class="mb-4"> Pengerjaan SKP : <span class="${skp} mb-2"> ${full['note_skp_bulanan']} </span> </span><br> 
                            <span class="mb-4"> Karya Ilmiah : <span class="${karya} mb-2"> ${full['note_bukti_karya_ilmiah']} </span>  </span><br> 
                        `
                    }
                },
                {
                    targets: 5,
                    orderable: true,
                    data: 'note_bukti_prestasi_siswa',
                    className: 'text-center',
                    render: function(data, type, full, meta) {
                        return full['note_supervisi_sekolah']
                    }
                },
                {
                    targets: 6,
                    orderable: true,
                    data: 'note_bukti_prestasi_siswa',
                    className: 'text-center',
                    render: function(data, type, full, meta) {
                        return full['note_bukti_prestasi_siswa'] == null ? '<span class="badge badge-soft-warning">Tidak ada</span>' : `<span class="badge badge-soft-secondary"> ${full['note_bukti_prestasi_siswa']} </span>`;
                    }
                },
                {
                    targets: 7,
                    orderable: false,
                    className: 'text-center',
                    // className: 'd-flex justify-content-center me-4',
                    render: function(data, type, full, meta) {
                        return `
                        <div class="dropdown d-inline-block">
                            <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="ri-more-fill align-middle"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" href="javascript:modalNilai('${full['id']}','modal-prangkat','Perangkat','perangkat');">Pengumpulan Perangkat</a></li>
                                <li><a class="dropdown-item" href="javascript:modalNilai('${full['id']}','modal-pengumpulan-nilai','Pengumpulan Nilai','pengumpulan');">Pengumpulan Nilai</a></li>
                                <li><a class="dropdown-item" href="javascript:modalBukti('${full['id']}','prestasi_siswa','Prestasi Siswa');">Upload prestasi siswa</a></li>
                                <li><a class="dropdown-item" href="javascript:modalBukti('${full['id']}','skp','SKP Bulanan');">Upload skp bulanan</a></li>
                                <li><a class="dropdown-item" data-pegawai="${btoa(JSON.stringify(full))}" href="javascript:void(0);" onclick="modalSupervisi(this)">Supervisi Sekolah</a></li>
                                <li><a class="dropdown-item" href="javascript:modalBukti('${full['id']}','karyailmiah','Karya Ilmiah');">Upload Karya Ilmiah</a></li>
                            </ul>
                        </div>
                        `;
                    }
                }
            ]
        })
    }

    function modalSupervisi(el) {
        var data = JSON.parse(atob($(el).data('pegawai')));
        $.each(data, (i, v) => {
            $(`[name="${i}"]`).val(v);
        })
        modalShow = 'modal-supervisi';
        $('#modal-supervisi').modal('show');
    }

    function onFilterPegawai() {
        selected_tahun = $('#filter_tahun option:selected').val()
        tahun = $("#filter_tahun option:selected").text();
        mainTable();
    }

    function comboYear() {
        app.ajax({
            dataType: 'json',
            url: URL + 'form/tahun/list',
            success: function(res) {
                $.each(res['tahun'], (i, v) => {
                    $('#filter_tahun').append(`<option value="${v['tahun']}">${v['tahun']}</option>`)
                    $('#tahun').append(`<option value="${v['tahun']}">${v['tahun']}</option>`)
                })
                $('#filter_tahun').select2();
                $('#tahun').select2();
                $('#filter_bulan').select2();
                setTimeout(() => {
                    selected_tahun = $('#filter_tahun option:selected').val()
                    // selected_bulan = $('#filter_bulan option:selected').val()
                    // title = $("#filter_bulan option:selected").text();
                    tahun = $("#filter_tahun option:selected").text();
                    mainTable()
                }, 400);
            }
        });
    }

    function openfile(URL){
        window.open(URL, '_blank');
    }
</script>