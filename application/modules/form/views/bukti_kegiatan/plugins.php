<script src="<?= base_url() ?>app-assets/js/app.js"></script>
<script>
    var peg_active = 0;
    $(() => {
        onListPegawai();
    })

    function onListPegawai(val = null) {
        $('#pegawaidata').html('');
        app.ajax({
            dataType: 'json',
            url: URL + 'form/bukti_kegiatan/listpeg',
            data: {
                'name': val
            },
            success: function(res) {
                $.each(res.listkegiatan, (i, v) => {
                    $('#tipe_bukti').append(`<option value="${v['kode']}">${v['nama']}</option>`)
                })
                $.each(res.pegawai, (i, v) => {
                    // $('#list-pegawai').append(`<li>
                    //     <a href="javascript:void(0)">
                    //         <i class="ri-file-list-2-line align-bottom me-2"></i> <span class="file-list-link">${v['nm_lkp_peg']}</span>
                    //     </a>
                    // </li>`);
                    $('#peg_id').append(`<option value="${v['id']}">${v['nm_lkp_peg']}</option>`)
                    $('#pegawaidata').append(`
                        <li>
                            <a data-bs-toggle="collapse" href="javascript:void(0)" onclick="show(this)" data-pegawai="${btoa(JSON.stringify(v))}" class="nav-link fs-14">${v['nm_lkp_peg']}</a>
                            <div class="collapse" id="${v['id']}">
                                <ul class="mb-0 sub-menu list-unstyled ps-3 vstack gap-2 mb-2">
                                    <li>
                                        <a href="#!"><i class="ri-stop-mini-fill align-middle fs-15 text-danger"></i> v2.1.0</a>
                                    </li>
                                    <li>
                                        <a href="#!"><i class="ri-stop-mini-fill align-middle fs-15 text-secondary"></i> v2.2.0</a>
                                    </li>
                                    <li>
                                        <a href="#!"><i class="ri-stop-mini-fill align-middle fs-15 text-info"></i> v2.3.0</a>
                                    </li>
                                    <li>
                                        <a href="#!"><i class="ri-stop-mini-fill align-middle fs-15 text-primary"></i> v2.4.0</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    `);
                })
                setTimeout(() => {
                    $('#peg_id,#tipe_bukti').select2({
                        dropdownParent: $("#modalform")
                    });
                    mainTable({
                        id: 0
                    })
                }, 400);
            },
        });
    }

    function modalform() {
        $('[name="file_upload_file"]').val('')
        $('#modalform').modal('show')
    }

    function search_peg(val) {
        onListPegawai(val);
    }

    function show(el) {
        var data = JSON.parse(atob($(el).data('pegawai')));
        $('#title-show').html(`<span class="badge bg-primary align-bottom ms-2">${data['nm_lkp_peg']}</span>`);
        peg_active = data['id'];
        mainTable({
            id: peg_active
        })
    }

    function mainTable(filter = {}) {
        $('#elmLoader').hide();
        app.tableAPI({
            el: 'table-data',
            url: APP_URL + 'form/bukti_kegiatan/maintable',
            data: filter,
            columnDefs: [{
                    targets: 1,
                    orderable: true,
                    data: 'bukti_nama',
                    render: function(data, type, full, meta) {
                        // return full['bukti_nama'];
                        return `<a target="_blank" href="${APP_URL+'content/view/bukti/'+full['id_bukti']}" >${full['bukti_nama']}</a>`
                    }
                },
                {
                    targets: 2,
                    orderable: true,
                    data: 'tanggal',
                    render: function(data, type, full, meta) {
                        return full['tanggal'];
                    }
                },
                {
                    targets: 3,
                    orderable: false,
                    // className: 'd-flex justify-content-end me-4',
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-outline-danger m-2 btn-sm" data-peg="'${full['peg_id']}'" data-id="${full['id_bukti']}" onclick="hapusData(this)">Delete</button>`;
                    }
                }
            ]
        })
    }

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        app.ajax({
            dataType: 'json',
            url: URL + 'form/bukti_kegiatan/save',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                if (res['success']) {
                    mainTable({
                        id: peg_active
                    })
                    // app.refresTable('main-table-data');
                    $('#modalform').modal('hide');
                }
            }
        });
    }

    function hapusData(el) {
        var data = $(el).data();
        app.confirmSave({
            url: URL + 'form/bukti_kegiatan/deletefile',
            data: {
                id: data['id']
            },
            success: function(res) {
                mainTable({
                    id: peg_active
                })
            }
        });
    }
</script>