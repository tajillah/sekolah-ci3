<div class="d-flex bd-highlight mb-3">
    <div class="mr-auto bd-highlight">
        <h4>Data Penilaian Kehadiran</h4>
    </div>
    <div class="bd-highlight">
        <!-- <button type="button" onclick="modalform()" class="btn btn-outline-primary mb-2">Tambah</button> -->
    </div>
    <!-- <div class="bd-highlight">Flex item</div> -->
</div>

<div class="card mb-4">
    <div class="card-body">
        <form action="javascript:onFilterPegawai()" autocomplete="off" id="form_filter_tamu" method="post">
            <div class="row">
                <div class="col-12 col-md">
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select id="filter_tahun" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="form-group">
                                <label>Bulan</label>
                                <select id="filter_bulan" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-2 align-self-center">
                    <button class="btn btn-sm btn-outline-primary mb-3 w-100" type="submit"><i class="fa fa-search"></i> Cari</button>
                    <!-- <button type="button" class="btn btn-sm btn-outline-secondary w-100" onclick="onFilterTamu(true)"><i class="fa fa-times"></i> Reset</button> -->
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <!-- table table-bordered dt-responsive nowrap table-striped align-middle dataTable no-footer dtr-inline collapsed -->
        <table id="main-table-data" class="table table-striped table-bordered nowrap table-striped align-middle dataTable">
            <thead class="table-light">
                <tr>
                    <th>NIP</th>
                    <th>NUPTK</th>
                    <th>NAMA</th>
                    <th>KEHADIRAN</th>
                    <th>HARI KERJA</th>
                    <th>SELAPAN</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


<form id="formData" action="javascript:saveIt('formData')">
    <div id="modalform" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Kehadiran <span class="title-modal"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">NIP</label>
                        <input type="text" readonly class="form-control" id="nip" name="nip" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label class="col-form-label">NAMA LENGKAP</label>
                        <input type="text" readonly class="form-control" required id="nm_lkp_peg" name="nm_lkp_peg" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label class="col-form-label">Total Kehadiran <span class="title-modal"></label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="number" class="form-control form-readonly" required id="kehadiran" onblur="match_predikat()" name="kehadiran">
                                <input type="hidden" class="form-control" id="hari_kerja" name="hari_kerja" placeholder="">
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" readonly id="predikat" name="predikat" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-form">
                        <label class="col-form-label">Upload Bukti Kehadiran</label>
                        <div class="row">
                            <div class="col-md-10">
                                <input type="file" class="form-control" name="file_upload_kehadiran" accept="image/*">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-outline-primary mb-2" onclick="fileUpload('formData')"><i class="las la-save"></i></button>
                            </div>
                        </div>
                    </div>
                    <div id="list-file-kehadiran"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary hidden-form">Save</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="formselapan" action="javascript:saveIt('formselapan')">
    <div id="modalselapan" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Selapan <span class="title-modal"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">NIP</label>
                        <input type="text" readonly class="form-control" id="nip" name="nip" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label class="col-form-label">NAMA LENGKAP</label>
                        <input type="text" readonly class="form-control" required id="nm_lkp_peg" name="nm_lkp_peg" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label class="col-form-label">Selapan <span class="title-modal"></label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="number" class="form-control form-readonly" onblur="match_selapan()" id="selapan_kbm" name="selapan_kbm" placeholder="">
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" readonly id="predikat_kbm" name="predikat_kbm" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden-form">
                        <label class="col-form-label">Upload Bukti Kehadiran</label>
                        <div class="row">
                            <div class="col-md-10">
                                <input type="file" class="form-control" name="file_upload_selapan" accept="image/*">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-outline-primary mb-2" onclick="fileUpload('formselapan')"><i class="las la-save"></i></button>
                            </div>
                        </div>
                    </div>
                    <div id="list-file-selapan"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary hidden-form">Save</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>