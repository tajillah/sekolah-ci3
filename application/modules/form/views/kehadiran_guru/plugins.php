<script>
    var taskAction = '';
    var selected_tahun = '01';
    var selected_bulan = '01';
    var title = '';
    var tahun = '';
    var modalopen = '';
    var taskUpload = '';
    $(() => {
        comboYear()
        if (USER_ROLE != 'admin') {
            $('.hidden-form').hide();
            $('.form-readonly').attr('readonly', true);
        }
    })

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        formData.append('tahun', selected_tahun);
        formData.append('bulan', selected_bulan);
        app.ajax({
            dataType: 'json',
            url: URL + 'form/KehadiranGuru/save',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                if (res['success']) {
                    app.refresTable('main-table-data');
                    $('#' + modalopen).modal('hide');
                }
            }
        });
    }

    function modalform(pID = null, modal = 'modalform', task) {
        modalopen = modal;
        taskUpload = task;
        $('.title-modal').html(title + `[${tahun}]`);
        $('#kehadiran').val('')
        $('#selapan_kbm').val('')
        if (USER_ROLE != 'admin') {
            $('.hidden-form').hide();
            $('.form-readonly').attr('readonly', true);
        }
        if (pID == null) {
            $('.form-control').val('')
            taskAction = 'store';
            $('#modalform').modal('show');
        } else {
            taskAction = 'update';
            app.ajax({
                dataType: 'json',
                url: URL + 'form/KehadiranGuru/show',
                data: {
                    'id': pID,
                    'taskUpload' : task
                },
                success: function(res) {
                    $.each(res, (i, v) => {
                        $(`[name="${i}"]`).val(v);
                    })
                    $('#list-file-'+taskUpload).html('');
                    if (res.total_file > 0) {
                        $.each(res.data_file, (i, v) => {
                            $('#list-file-'+taskUpload).append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)
                        })
                    }
                    setTimeout(() => {
                        match_predikat()
                    }, 500);
                    $('#' + modalopen).modal('show');
                }
            });
        }
    }

    function openfile(URL) {
        window.open(URL, '_blank');
    }

    function fileUpload(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        formData.append('taskUpload',taskUpload);
        app.ajax({
            dataType: 'json',
            url: URL + 'form/KehadiranGuru/upload',
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                $('#list-file-'+taskUpload).html('');
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-'+taskUpload).append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span onclick="openfile('${APP_URL+'content/view/bukti/'+v['id_bukti']}')" class="badge badge-label bg-primary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';>
                                        <i class="mdi mdi-circle-medium"></i> ${v.bukti_nama}
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="las la-trash-alt"></i></button>
                                </div>
                            </div>`)
                    })
                }
            }
        });
    }

    function delFile(id, parent_id) {
        app.confirmSave({
            url: URL + 'form/KehadiranGuru/deletefile',
            data: {
                id: id,
                parent_id: parent_id
            },
            success: function(res) {
                $('#list-file-'+taskUpload).html('');
                if (res.total_file > 0) {
                    $.each(res.data_file, (i, v) => {
                        $('#list-file-'+taskUpload).append(`
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <span class="badge outline-badge-secondary" onMouseOver="this.style.cursor='pointer';" onMouseOut="this.style.cursor='pointer';"> 
                                    ${v.bukti_nama} 
                                    </span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-outline-danger mb-2" onclick="delFile('${v.id_bukti}','${v.parent_id}')"><i class="far fa-trash-alt"></i></button>
                                </div>
                            </div>`)
                    })
                }
            }
        });
    }

    function mainTable() {
        app.tableAPI({
            el: 'main-table-data',
            url: APP_URL + 'form/KehadiranGuru/maintable',
            data: {
                tahun: selected_tahun,
                bulan: selected_bulan
            },
            columnDefs: [{
                    targets: 0,
                    orderable: true,
                    visible: false,
                    data: 'nip',
                    render: function(data, type, full, meta) {
                        return full['nip'];
                    }
                },
                {
                    targets: 1,
                    orderable: true,
                    data: 'nuptk',
                    render: function(data, type, full, meta) {
                        return `<div class="flex-grow-1"><h5 class="fs-14 mb-1">
                            <a href="javascript:void(0)" class="text-dark">${full['nuptk'] == null ? '-' : full['nuptk']}</a>
                            </h5><p class="text-muted mb-0">NIP : <span class="fw-medium">${full['nip'] == null ? '-' : full['nip']}</span></p>
                            </div>`
                    }
                },
                {
                    targets: 2,
                    orderable: true,
                    data: 'nm_lkp_peg',
                    render: function(data, type, full, meta) {
                        return full['nm_lkp_peg'];
                    }
                },
                {
                    targets: 3,
                    orderable: true,
                    data: 'kehadiran',
                    className: 'text-center',
                    render: function(data, type, full, meta) {
                        return full['kehadiran'] + ' Hari';
                    }
                },
                {
                    targets: 4,
                    orderable: true,
                    data: 'hari_kerja',
                    className: 'text-center',
                    render: function(data, type, full, meta) {
                        return full['hari_kerja'] + ' Hari';
                    }
                },
                {
                    targets: 5,
                    orderable: true,
                    data: 'selapan_kbm',
                    className: 'text-center',
                    render: function(data, type, full, meta) {
                        return full['selapan_kbm'];
                    }
                },
                {
                    targets: 6,
                    orderable: false,
                    className: 'text-center',
                    render: function(data, type, full, meta) {
                        return `
                        <div class="dropdown d-inline-block">
                            <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="ri-more-fill align-middle"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" href="javascript:modalform('${full['id']}','modalform','kehadiran');">Kehadiran</a></li>
                                <li><a class="dropdown-item" href="javascript:modalform('${full['id']}','modalselapan','selapan');">Selapan</a></li>
                            </ul>
                        </div>
                        `;
                        // return `<button class="btn btn-outline-primary m-2 btn-sm" onclick="modalform('${full['id']}')">Edit</button>`;
                    }
                }
            ]
        })
    }

    function onFilterPegawai() {
        selected_tahun = $('#filter_tahun option:selected').val()
        selected_bulan = $('#filter_bulan option:selected').val()
        title = $("#filter_bulan option:selected").text();
        tahun = $("#filter_tahun option:selected").text();
        mainTable();
    }

    function comboYear() {
        app.ajax({
            dataType: 'json',
            url: URL + 'form/tahun/list',
            success: function(res) {
                $.each(res['tahun'], (i, v) => {
                    $('#filter_tahun').append(`<option value="${v['tahun']}">${v['tahun']}</option>`)
                    $('#tahun').append(`<option value="${v['tahun']}">${v['tahun']}</option>`)
                })
                $.each(res['bulan'], (i, v) => {
                    $('#filter_bulan').append(`<option value="${v['bulan']}">${v['nama']}</option>`)
                })
                $('#filter_tahun').select2();
                $('#tahun').select2();
                $('#filter_bulan').select2();
                setTimeout(() => {
                    selected_tahun = $('#filter_tahun option:selected').val()
                    selected_bulan = $('#filter_bulan option:selected').val()
                    title = $("#filter_bulan option:selected").text();
                    tahun = $("#filter_tahun option:selected").text();
                    mainTable()
                }, 400);
            }
        });
    }

    function match_predikat() {
        var hadir = $('#kehadiran').val();
        var hari_kerja = $('#hari_kerja').val();
        var total = parseInt(hari_kerja) - parseInt(hadir);
        var predikat = 'K';
        if (total <= 5) {
            predikat = 'SB';
        } else if (total <= 10) {
            predikat = 'B';
        } else if (total <= 15) {
            predikat = 'C';
        }
        $('#predikat').val(predikat)
    }

    function match_selapan() {
        var total = $('#selapan_kbm').val();
        var predikat = 'K';
        if (total <= 5) {
            predikat = 'SB';
        } else if (total <= 10) {
            predikat = 'B';
        } else if (total <= 15) {
            predikat = 'C';
        }
        $('#predikat_kbm').val(predikat)
    }
</script>