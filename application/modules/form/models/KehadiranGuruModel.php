<?php
class KehadiranGuruModel extends MY_Model
{
    protected $table = 'pegawai';
    protected $primary_key = 'id';
    public function __construct()
    {
        parent::__construct();
    }

    function generate(){
        $data = request();
        $aArrPegawai = $this->db->get('pegawai')->result_array();
        $save['bulan']  = $data['bulan'];
        $save['tahun']  = $data['tahun'];
        $getBulan = $this->db->get_where('bulan',$save)->row_array();
        foreach ($aArrPegawai as $key => $value) {
            $save = [];
            $save['bulan']  = $data['bulan'];
            $save['tahun']  = $data['tahun'];
            $save['peg_id'] = $value['id'];
            $cek = $this->db->get_where('kehadiran_rekap',$save)->row_array();
            
            $save['hari_kerja']  = $getBulan['hari_kerja'];
            if(empty($cek)){
                $save['kehadiran']  = 0;
                $save['tidak_hadir'] = $save['hari_kerja'] - $save['kehadiran'];
                $save['id'] = gen_uuid();
                $this->db->insert('kehadiran_rekap',$save);
            }else{
                $save['tidak_hadir'] = $save['hari_kerja'] - $cek['kehadiran'];
                $this->db->update('kehadiran_rekap',$save,['id' => $cek['id']]);
            }
        }
    }
}
