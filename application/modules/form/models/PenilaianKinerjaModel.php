<?php
class PenilaianKinerjaModel extends MY_Model
{
    protected $table = 'pegawai';
    protected $primary_key = 'id';
    public function __construct()
    {
        parent::__construct();
    }

    function generate()
    {
        $data = request();
        $aArrPegawai = $this->db->get('pegawai')->result_array();
        foreach ($aArrPegawai as $key => $value) {
            $save = [];
            $save['tahun']  = $data['tahun'];
            $save['peg_id'] = $value['id'];
            $cek = $this->db->get_where('nilai_asn_tahun', $save)->row_array();

            if (empty($cek)) {
                $save['nilai_perangkat'] = 0;
                $save['nilai_pengumpulan'] = 0;
                $save['nilai_perangkat_predikat'] = 'K';
                $save['nilai_pengumpulan_predikat'] = 'K';
                $save['note_skp_bulanan'] = 'Tidak Sesuai deadline';
                $save['note_bukti_prestasi_siswa'] = 'Tidak ada';
                $save['note_bukti_karya_ilmiah'] = 'Tidak ada';
                $save['nilai_predikat_guru'] = null;
                $save['id'] = gen_uuid();
                $this->db->insert('nilai_asn_tahun', $save);
            } else {
                $this->db->update('nilai_asn_tahun', $save, ['id' => $cek['id']]);
            }
        }
    }
}
