<?php defined('BASEPATH') or exit('No direct script access allowed');
class Content extends AppController
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model(['BulanModel' => 'Bulan']);
    }

    function index($from = null, $source = null)
    {
        switch ($from) {
            case 'bukti':
                $read = $this->db->get_where('bukti_kinerja_guru', ['id_bukti' => $source])->row_array();
                // echo $read['file_patch'] . $read['bukti_file_name'];
                $this->showPreviewContent('jpeg', $read['file_patch'] . $read['bukti_file_name'], $read['bukti_file_name']);
                break;
            case 'img-user':
                $read = $this->db->get_where('pegawai', ['id' => $this->session->user['biodata_id']])->row_array();
                if(!empty($read['avatar'])){
                    $this->showPreviewContent('jpeg', './dokumen/pegawai/avatar/' .$this->session->user['biodata_id'].'/'. $read['avatar'], $read['avatar']);
                }else{
                    $this->showPreviewContent('jpeg', './app-assets/images/users/avatar-1.jpg');
                }
                break;
            default:
                break;
        }
    }


    protected function showPreviewContent($type = "jpeg", $file = null, $file_name = null)
    {
        if ($type == "jpg" || $type == "png") {
            $type = "jpeg";
        }
        if ($file) {
            try {
                $check_exist = file_exists($file);
                if ($check_exist) {
                    $get_content = file_get_contents($file);
                    if ($get_content) {
                        if ($type == "jpeg" || $type == "pdf") {
                            $this->output->set_content_type($type)
                                ->set_output($get_content);
                            return true;
                        } else {
                            $this->_push_file($file, $file_name);
                        }
                    }
                }
            } catch (Exception $e) {
                show_404();
            }
        }

        if ($type == "jpeg") {
            $this->output->set_content_type('jpeg')->set_output(file_get_contents('dokumen/noimage.png'));
        } elseif ($type == "pdf") {
            $this->output->set_content_type('pdf')->set_output(file_get_contents('dokumen/nopdf.mp4'));
        } else {
            show_404();
        }
    }

    protected function _push_file($path, $name)
    {
        if (is_file($path)) {
            if (ini_get('zlib.output_compression')) {
                ini_set('zlib.output_compression', 'Off');
            }
            $mime = get_mime_by_extension($path);

            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
            header('Cache-Control: private', false);
            header('Content-Type: ' . $mime);
            header('Content-Disposition: attachment; filename="' . basename($name) . '"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($path));
            header('Connection: close');
            readfile($path);
            exit();
        }
    }
}
