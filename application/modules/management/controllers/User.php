<?php defined('BASEPATH') or exit('No direct script access allowed');
class User extends AppController
{
    protected $table = 'app_shared';
    protected $primary_key = 'user_id';
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['ConfigModel' => 'configDB']);
    }

    public function maintable()
    {
        $res = $this->datatable
            ->table($this->table)
            ->draw();
        $this->response($res);
    }

    public function show()
    {
        $data = request();
        $read = $this->configDB->table('app_shared')->read(['user_id' => $data['id']]);
        $this->response($read);
    }

    function update()
    {
        $data = request();
        $data['password'] = trim($data['password']);
        if (empty($data['password']) || $data['password'] == '') {
            unset($data['password']);
        } else {
            $data['password'] = password_app($data['password']);
        }
        $res = $this->configDB->table('app_shared')->edit(['user_id' => $data['user_id']], $data);
        $res = $this->configDB->table('pegawai')->edit(['user_id' => $data['user_id']], ['email' => $data['email']]);
        $this->response($res);
    }

    function delete()
    {
        $data = request();
        $this->db->delete('app_role_user', ['user_id' => $data['id']]);
        $this->db->update('pegawai', ['user_id' => null], ['user_id' => $data['id']]);
        $res = $this->configDB->table('app_shared')->destroy(['user_id' => $data['id']]);
        $this->response($res);
    }


    function generate()
    {
        $data = request();
        $res = false;
        $msgText = '';
        $pegawai = $this->db->get_where('pegawai', ['id' => $data['id']])->row_array();
        // echo 'asdasd';
        // print_r($pegawai);
        // exit;
        if (!empty($pegawai['email'])) {
            $save['email']  = $pegawai['email'];
            if (empty($pegawai['user_id'])) {
                $pass  = explode('@', $pegawai['email']);
                $save['user_full_name']  = $pegawai['nm_lkp_peg'];
                $save['password']       = password_app($pass[0]);
                $save['biodata_id']      = $data['id'];
                $save['is_active']      = 'enable';
                $save['user_role_url']  = 'guru';
                $save['user_role_id']   = 'ecc400acb3cd9dba7c66145f37f9e635';
                $save['user_id']        = gen_uuid();
                $res = $this->db->insert('app_shared', $save);
                $res = $this->db->update('pegawai', ['user_id' => $save['user_id']], ['id' => $data['id']]);
                $saveRoleMain['id']  = gen_uuid();
                $saveRoleMain['user_id']    = $save['user_id'];
                $saveRoleMain['role_id']    = 'ecc400acb3cd9dba7c66145f37f9e635';
                $saveRoleMain['role_url']   = 'guru';
                $saveRoleMain['role_name']  = 'Guru';
                $saveRoleMain['role_main']  = '1';
                $res = $this->db->insert('app_role_user', $saveRoleMain);
                $msgText = 'Berhasil membuat akun login';
            } else {
                // $res = $this->db->update('app_shared', $save,['user_id' => $pegawai['user_id']]);
                $msgText = 'Akun sudah di generate';
            }
        } else {
            $msgText = 'Email pegawai belum terisi';
        }
        $response['success']    = $res;
        $response['status']     = $res ? 'success' : 'error';
        $response['message']    = $msgText;
        $this->response($response);
    }
}
