<?php defined('BASEPATH') or exit('No direct script access allowed');
class Pegawai extends AppController
{
    protected $table = 'pegawai';
    protected $primary_key = 'id';
    public function __construct()
    {
        parent::__construct();
		$this->load->model(['PegawaiModel' => 'Pegawai']);
    }

    public function maintable()
    {
        $res = $this->datatable
            ->table($this->table)
            ->draw();
        $this->response($res);
    }

    public function show(){
        $data = request();
        $read = $this->Pegawai->read($data['id']);
        $this->response($read);
    }

    function store(){
        $data = request();
        $data['id'] = gen_uuid();
        $data['akun_status'] = 0;
        $res = $this->Pegawai->store($data,gen_uuid());
        $this->response($res);
    }

    function update(){
        $data = request();
        $res = $this->Pegawai->edit($data['id'],$data);
        $this->response($res);
    }

    function delete(){
        $data = request();
        $res = $this->Pegawai->destroy($data['id']);
        $this->response($res);
    }
}
