<script>
    var taskAction = '';
    $(() => {
        mainTable()
    })

    function hapusData(id) {
        app.confirmSave({
            url: URL + 'management/pegawai/delete',
            data: {
                id: id
            },
            success: function(res) {
                app.refresTable('main-table-data');
            }
        });
    }

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        app.ajax({
            dataType: 'json',
            url: URL + 'management/pegawai/' + taskAction,
            data: formData,
            contentType: false,
            processData: false,
            message: true,
            success: function(res) {
                if (res['success']) {
                    app.refresTable('main-table-data');
                    $('#modalform').modal('hide');
                }
            }
        });
    }

    function modalform(pID = null) {
        if (pID == null) {
            $('.form-control').val('')
            taskAction = 'store';
            $('#modalform').modal('show');
        } else {
            taskAction = 'update';
            app.ajax({
                dataType: 'json',
                url: URL + 'management/pegawai/show',
                data: {
                    'id': pID
                },
                success: function(res) {
                    $.each(res, (i, v) => {
                        $(`[name="${i}"]`).val(v);
                    })
                    $('#modalform').modal('show');
                }
            });
        }
    }

    function mainTable() {
        app.tableAPI({
            el: 'main-table-data',
            url: APP_URL + 'management/pegawai/maintable',
            columnDefs: [{
                    targets: 1,
                    orderable: true,
                    data: 'nip',
                    visible:false,
                    render: function(data, type, full, meta) {
                        return full['nip'];
                    }
                },
                {
                    targets: 2,
                    orderable: true,
                    data: 'nuptk',
                    render: function(data, type, full, meta) {
                        // return full['nuptk'];
                        return `<div class="flex-grow-1"><h5 class="fs-14 mb-1">
                            <a href="javascript:void(0)" class="text-dark">${full['nuptk'] == null ? '-' : full['nuptk']}</a>
                            </h5><p class="text-muted mb-0">NIP : <span class="fw-medium">${full['nip'] == null ? '-' : full['nip']}</span></p>
                            </div>`
                    }
                },
                {
                    targets: 3,
                    orderable: true,
                    data: 'nm_lkp_peg',
                    render: function(data, type, full, meta) {
                        // return full['nm_lkp_peg'];
                        return `<div class="flex-grow-1"><h5 class="fs-14 mb-1">
                            <a href="javascript:void(0)" class="text-dark">${full['nm_lkp_peg'] == null ? '-' : full['nm_lkp_peg']}</a>
                            </h5><p class="text-muted mb-0">Golongan : <span class="fw-medium">${full['golongan'] == null ? '-' : full['golongan']}</span></p>
                            </div>`
                    }
                },
                {
                    targets: 4,
                    orderable: true,
                    data: 'golongan',
                    visible:false,
                    render: function(data, type, full, meta) {
                        return full['golongan'];
                    }
                },
                {
                    targets: 5,
                    orderable: true,
                    data: 'email',
                    render: function(data, type, full, meta) {
                        return full['email'];
                    }
                },
                {
                    targets: 6,
                    orderable: false,
                    // className: 'd-flex justify-content-center me-4',
                    render: function(data, type, full, meta) {
                        generate = '';
                        if(full['user_id'] == null){
                            generate = `<li><a class="dropdown-item" href="javascript:generateAkun('${full['id']}')">Generate Akun</a></li>`;
                        }
                        return `
                        <div class="dropdown d-inline-block">
                            <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="ri-more-fill align-middle"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" href="javascript:modalform('${full['id']}')">Edit</a></li>
                                ${generate}
                            </ul>
                        </div>
                        `;
                        // return `<button class="btn btn-outline-primary m-2 btn-sm" onclick="modalform('${full['id']}')">Edit</button>
                        // <button class="btn btn-outline-danger m-2 btn-sm" onclick="hapusData('${full['id']}')">Delete</button>`;
                    }
                }
            ]
        })
    }


    function generateAkun(id) {
        app.confirmSave({
            url: URL + 'management/user/generate',
            data: {
                id: id
            },
            title: 'Generate Akun User',
            description: 'Akan membuatkan akun user yang akan digunakan untuk login',
            buttonText: 'Generate',
            message : false,
            success: function(res) {
                console.log(res)
                if (res.success) {
                    app.refresTable('main-table-data');
                }
                Swal.fire({
                    icon: res.status,
                    // title: 'Oops...',
                    text: res.message,
                })
            }
        });
    }
</script>