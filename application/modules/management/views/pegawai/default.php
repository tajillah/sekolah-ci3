<div class="page-title-box d-sm-flex align-items-center justify-content-between">
    <h4 class="mb-sm-0" id="title-pages">Data Pegawai</h4>

    <div class="page-title-right">
        <button type="button" onclick="modalform()" class="btn btn-outline-primary mb-2">Tambah</button>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <table id="main-table-data" class="table table-striped table-bordered nowrap table-striped align-middle dataTable">
            <thead>
                <tr class="table-light">
                    <th>No</th>
                    <th>NIP</th>
                    <th>NUPTK</th>
                    <th>NAMA</th>
                    <th>PANGKAT/GOLONGAN</th>
                    <th>EMAIL</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


<form id="formData" action="javascript:saveIt('formData')">
    <div id="modalform" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Pegawai</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">NIP</label>
                        <input type="text" class="form-control" id="nip" name="nip" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="nm_lkp_peg" class="col-form-label">NAMA LENGKAP</label>
                        <input type="text" class="form-control" required id="nm_lkp_peg" name="nm_lkp_peg" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="nuptk" class="col-form-label">NUPTK</label>
                        <input type="text" class="form-control" id="nuptk" name="nuptk" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="golongan" class="col-form-label">GOLONGAN</label>
                        <input type="text" class="form-control" id="golongan" name="golongan" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label for="email" class="col-form-label">EMAIL</label>
                        <input type="text" class="form-control" required id="email" name="email" placeholder="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>