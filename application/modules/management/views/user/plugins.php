<script>
    $(() => {
        mainTable()
    })

    function mainTable() {
        app.tableAPI({
            el: 'main-table-data',
            url: APP_URL + 'management/User/maintable',
            columnDefs: [{
                    targets: 1,
                    orderable: true,
                    data: 'user_full_name',
                    render: function(data, type, full, meta) {
                        return full['user_full_name'];
                    }
                },
                {
                    targets: 2,
                    orderable: true,
                    data: 'email',
                    render: function(data, type, full, meta) {
                        return full['email'];
                    }
                },
                {
                    targets: 3,
                    orderable: true,
                    data: 'is_active',
                    render: function(data, type, full, meta) {
                        return full['is_active'];
                    }
                },
                {
                    targets: 4,
                    orderable: true,
                    data: 'user_role_url',
                    render: function(data, type, full, meta) {
                        return full['user_role_url'];
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    // className: 'd-flex justify-content-center me-4',
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-outline-primary m-2 btn-sm" onclick="modalform('${full['user_id']}')">Edit</button>
                        <button class="btn btn-outline-danger m-2 btn-sm" onclick="hapusData('${full['user_id']}')">Delete</button>`;
                    }
                }
            ]
        })
    }

    function hapusData(id) {
        app.confirmSave({
            url: URL + 'management/user/delete',
            data: {
                id: id
            },
            success: function(res) {
                app.refresTable('main-table-data');
            }
        });
    }

    function modalform(pID = null) {
        $('.form-control').val('')
        app.ajax({
            dataType: 'json',
            url: URL + 'management/user/show',
            data: {
                'id': pID
            },
            success: function(res) {
                $.each(res, (i, v) => {
                    if (i == 'email' || i == 'user_id') {
                        $(`[name="${i}"]`).val(v);
                    }
                })
                $('#modalform').modal('show');
            }
        });
    }

    function saveIt(name) {
        var form = $(`#${name}`)[0];
        var formData = new FormData(form);
        app.ajax({
            dataType: 'json',
            url: URL + 'management/user/update',
            data: formData,
            contentType: false,
            processData: false,
            message:true,
            success: function(res) {
                if (res['success']) {
                    app.refresTable('main-table-data');
                    $('#modalform').modal('hide');
                }
            }
        });
    }
</script>