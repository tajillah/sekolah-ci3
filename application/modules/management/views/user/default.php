<div class="page-title-box d-sm-flex align-items-center justify-content-between">
    <h4 class="mb-sm-0" id="title-pages">DAFTAR AKUN USER</h4>
    <div class="page-title-right">
        <!-- <button type="button" onclick="modalform()" class="btn btn-outline-primary mb-2">Tambah</button> -->
    </div>
</div>
<div class="card">
    <div class="card-body">
        <table id="main-table-data" class="table table-striped table-bordered nowrap table-striped align-middle dataTable">
            <thead>
                <tr class="table-light">
                    <th>NO</th>
                    <th>NAMA PEGAWAI</th>
                    <th>EMAIL</th>
                    <th>STATUS</th>
                    <th>MAIN ROLE</th>
                    <th>ACTIONS</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>


<form id="formData" action="javascript:saveIt('formData')">
    <div id="modalform" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form User</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="user_id" value="">
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">EMAIL</label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group mb-4">
                        <label for="nip" class="col-form-label">PASSWORD</label>
                        <input type="text" class="form-control" id="password" name="password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>