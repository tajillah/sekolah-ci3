<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'panel';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['login']     = 'auth/index';
$route['do_login']  = 'auth/do_login';
$route['logout']    = 'panel/logout';
$route['content/view/(:any)/(:any)']    = 'content/index/$1/$2';