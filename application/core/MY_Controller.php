<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * CodeIgniter-HMVC
 *
 * @package    CodeIgniter-HMVC
 * @author     N3Cr0N (N3Cr0N@list.ru)
 * @copyright  2019 N3Cr0N
 * @license    https://opensource.org/licenses/MIT  MIT License
 * @link       <URI> (description)
 * @version    GIT: $Id$
 * @since      Version 0.0.1
 * @filesource
 *
 */

class MY_Controller extends MX_Controller
{
    //
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout,....
     */
    protected $data = array();

    /**
     * [__construct description]
     *
     * @method __construct
     */
    public function __construct()
    {
        // To inherit directly the attributes of the parent class.
        parent::__construct();

        // This function returns the main CodeIgniter object.
        // Normally, to call any of the available CodeIgniter object or pre defined library classes then you need to declare.
        $CI =& get_instance();

        // Copyright year calculation for the footer
        $begin = 2019;
        $end =  date("Y");
        $date = "$begin - $end";

        // Copyright
        $this->data['copyright'] = $date;
        $this->item_app();
        // $this->load->model(['config/ConfigModel' => 'configDB']);
    }

    public function response($data = [], $tipe = false)
    {
        if ($tipe == true) {
            $response['record']     = $data;
            $response['success']    = empty($data['success']) ? true : $data['success'];
            $response['total']      = empty($data) ? 0 : count($data);
            $response['status']     = empty($data['status']) ? 'success' : $data['status'];
            $response['message']    = empty($data['message']) ? 'Berhasil Memproses Data!!' : $data['message'];
        } else {
            if (is_array($data)) {
                $response = empty($data) ?: $data;
            } else {
                $response = $data;
            }
        }
        // $response['isLogin']    = empty($this->session->user['isLogin']) ? false : true;

        $this->output
            ->set_content_type('css', 'utf-8')
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    function item_app()
    {
        $cnf = $this->db->get('app_config')->result_array();
        if (count($cnf) > 0) {
            foreach ($cnf as $key => $value) {
                $this->config->set_item($value['app_kode'], $value['app_value']);
            }
        }
    }
}

// Backend controller
// require_once(APPPATH.'core/Backend_Controller.php');

// Frontend controller
// require_once(APPPATH.'core/Frontend_Controller.php');

require_once(APPPATH.'core/App_Controller.php');
