<?php
class MY_Model extends CI_Model
{

    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */

    /**
     * This model's default database table. Automatically
     * guessed by pluralising the model name.
     */
    protected $table;

    /**
     * The database connection object. Will be set to the default
     * connection. This allows individual models to use different DBs
     * without overwriting CI's global $this->db connection.
     */
    public $_database;

    /**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */

    protected $primary_key = 'id';
    protected $whereFields;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function store($saving = [], $key = null)
    {
        $fields = $this->db->list_fields($this->table);
        $tempSave = [];
        $bStatusSave = false;

        if (is_array($key)) {
            $tempSave = $key;
        } else {
            $key = is_null($key) ? gen_uuid() : $key;
            $tempSave[$this->primary_key] = $key;
        }
        $tempSave = array_merge($tempSave, $saving);
        $save = [];
        $failed = [];
        foreach ($tempSave as $key => $value) {
            foreach ($fields as $field) {
                if ($field == $key) {
                    $save[$key] = $value;
                } else {
                    $failed[$key] = $value;
                }
            }
        }
        $bStatusSave = $this->db->insert($this->table, $save);

        $response['success']    = $bStatusSave;
        $response['status']     = $bStatusSave ? 'success' : 'error';
        $response['message']    = $bStatusSave ? 'Berhasil Menambah Data!!' : 'Gagal Menambah Data!!';
        $response['record']     = $save;
        $response['failed']     = $failed;
        $response['id']         = $key;
        return $response;
    }

    public function edit($key = null, $aArrPost = [])
    {
        $table = $this->table;
        $fields = $this->db->list_fields($table);
        $bStatusSave = false;
        $tempSave = [];

        if (is_array($key)) {
            $where = $key;
        } else {
            $where[$this->primary_key] = $key;
        }

        $save = [];
        foreach ($aArrPost as $key => $value) {
            foreach ($fields as $field) {
                if ($field == $key) {
                    $save[$key] = $value;
                }
            }
        }
        $bStatusSave = $this->db->update($table, $save, $where);

        $response['success']    = $bStatusSave;
        $response['status']     = $bStatusSave ? 'success' : 'error';
        $response['message']    = $bStatusSave ? 'Berhasil Memperbarui Data!!' : 'Gagal Memperbarui Data!!';
        $response['record']     = $save;
        $response['id']         = $key;
        $response['where']      = $where;
        return $response;
    }

    public function read($aArrData = [])
    {
        if ($aArrData == '' && is_null($aArrData)) {
            return [];
        } else {
            if (!empty($aArrData['select'])) {
                foreach ($aArrData['select'] as $select) {
                    $this->db->select($select);
                }
            }
            if (is_array($aArrData)) {
                foreach ($aArrData as $fild => $where) {
                    $this->db->where([$fild => $where]);
                }
            } else {
                $this->db->where([$this->primary_key => $aArrData]);
            }
            $this->db->from($this->table);
            $res = $this->db->get()->row_array();
            $opr = (is_null($res)) ? ['success' => (is_null($res)) ? false : true] : array_merge($res, ['success' => (is_null($res)) ? false : true]);
            return $opr;
        }
    }

    public function destroy($key)
    {
        $table = $this->table;
        if (is_array($key)) {
            $where = $key;
        } else {
            $where[$this->primary_key] = $key;
        }
        $bStatusSave = $this->db->delete($table, $where);

        $response['success']    = $bStatusSave;
        $response['status']     = $bStatusSave;
        $response['status']     = $bStatusSave ? 'success' : 'error';
        $response['message']    = $bStatusSave ? 'Berhasil Menghapus Data!!' : 'Gagal Menghapus Data!!';
        $response['id']         = $key;
        $response['where']      = $where;
        return $response;
    }

    /**
     * Getter for the table name
     */
    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function where($data)
    {
        $this->db->where($data);
        foreach ($data as $field => $value) {
            $this->whereFields[] = $field;
        }
        $this->whereData = $data;
        return $this;
    }
}
