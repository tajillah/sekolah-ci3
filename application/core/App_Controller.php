<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * CodeIgniter-HMVC
 *
 * @package    CodeIgniter-HMVC
 * @author     N3Cr0N (N3Cr0N@list.ru)
 * @copyright  2019 N3Cr0N
 * @license    https://opensource.org/licenses/MIT  MIT License
 * @link       <URI> (description)
 * @version    GIT: $Id$
 * @since      Version 0.0.1
 * @filesource
 *
 */

class AppController extends MY_Controller
{
    protected $table;
    protected $folder;
    protected $modules;
    //
    public $CI;

    /**
     * An array of variables to be passed through to the
     * view, layout, ....
     */
    protected $data = array();

    /**
     * [__construct description]
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct();
        $this->setCors();
        // $this->output->enable_profiler(true);
        // $CI = &get_instance();
        if (empty($this->session->user['isLogin'])) {
            if (strtolower($this->uri->segment(1)) != 'login' && strtolower($this->uri->segment(1)) != 'do_login' && strtolower($this->uri->segment(1)) != 'auth') {
                redirect(base_url('login'));
            }
        }
        $this->item_app();
    }

    private function setCors()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400"); //cache 1 day
        header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
    }

    public function response($data = [], $tipe = false)
    {
        if ($tipe == true) {
            $response['record']     = $data;
            $response['success']    = empty($data['success']) ? true : $data['success'];
            $response['total']      = empty($data) ? 0 : count($data);
            $response['status']     = empty($data['status']) ? 'success' : $data['status'];
            $response['message']    = empty($data['message']) ? 'Berhasil Memproses Data!!' : $data['message'];
        } else {
            if (is_array($data)) {
                $response = empty($data) ?: $data;
            } else {
                $response = $data;
            }
        }
        // $response['isLogin']    = empty($this->session->user['isLogin']) ? false : true;

        $this->output
            ->set_content_type('css', 'utf-8')
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    function item_app()
    {
        $cnf = $this->db->get('app_config')->result_array();
        if (count($cnf) > 0) {
            foreach ($cnf as $key => $value) {
                $this->config->set_item($value['app_kode'], $value['app_value']);
            }
        }
    }
}
