<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('password_app')) {
    function password_app($teks)
    {
        $teks = md5($teks);
        return md5($teks . 'Nu');
    }
}

if (!function_exists('request')) {
    function request($aArrReq = null, $typ = 'post')
    {
        $CI = &get_instance();
        $aArrTemp = [];
        $data = $CI->input->$typ();
        if (!empty($data)) {
            foreach ($data as $name => $v) {
                if (is_null($aArrReq)) {
                    $aArrTemp[$name] = $v;
                } else {
                    if (!is_array($aArrReq)) {
                        if ($aArrReq == $name) {
                            $aArrTemp = $v;
                        }
                    } else {
                        for ($i = 0; $i < count($aArrReq); $i++) {
                            $nm = $aArrReq[$i];
                            if ($name == $nm) {
                                $aArrTemp[$name] = $v;
                            }
                        }
                    }
                }
            }
        }
        if (!is_null($aArrReq)) {
            $aArrTemp = empty($aArrTemp) ? null : $aArrTemp;
        }
        return $aArrTemp;
    }
}

if (!function_exists('gen_uuid_key')) {
    function gen_uuid_key()
    {
        $CI = &get_instance();
        $a = $CI->db->select("UUID() AS UIDD")->get()->row_array();
        return md5($a['UIDD']);
    }
}

if (!function_exists('gen_uuid')) {
    function gen_uuid()
    {
        return md5(sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        ));
    }
}
