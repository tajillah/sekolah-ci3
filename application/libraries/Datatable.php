<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datatable
{
    private $db;
    private $input;
    private $table;
    private $alias = [];
    private $whereFields = [];
    private $whereData;
    private $search;
    private $joins = [];

    public function __construct()
    {
        $ci = &get_instance();
        $this->db = $ci->db;
        $this->input = $ci->input;
    }

    private function get_limiting()
    {
        $limit = $this->input->post('length', true);
        $start = $this->input->post('start', true);
        $this->db->limit($limit, $start);
    }

    // private function get_result()
    // {
    //     $this->get_limiting();
    //     // return $this->db->get($this->table)->result_array();
    //     return $this->db->get($this->table) ? $this->db->get($this->table)->result_array() : [];
    // }

    private function get_ordering()
    {
        $orderField = $this->input->post('order')[0]['column'];
        
        $orderAD = $this->input->post('order')[0]['dir'];
        $orderColumn = $this->input->post('columns')[$orderField]['data'];
        print_r($orderAD);
        exit;
        $this->db->order_by($orderColumn, $orderAD);
    }

    // private function get_filtering($keyword)
    // {
    //     $fields = $this->input->post('columns');

    //     $this->db->group_start();
    //     for ($i = 0; $i < count($fields); $i++) {
    //         $where = false;
    //         $field = $fields[$i]['data'];
    //         foreach ($this->whereFields as $data) {
    //             $where = ($field == $data) ? true : false;
    //         }
    //         if ($where) continue;
    //         if (array_key_exists($field, $this->alias)) {
    //             $field = $this->alias[$field];
    //             ($i < 1) ? $this->db->like($field, $keyword) : $this->db->or_like($field, $keyword);
    //         } else {
    //             ($i < 1) ? $this->db->like($field, $keyword) : $this->db->or_like($field, $keyword);
    //         }
    //     }
    //     $this->db->group_end();
    // }
    private function get_filtering()
    {
        $keyword = empty($this->input->post('search')['value']) ? '' : $this->input->post('search')['value'];
        $where = [];
        if (strlen($keyword) > 0) {
            $qs = '';
            $fields = $this->db->list_fields($this->table);
            foreach ($fields as $field) {
                $qs = $qs == '' ? '(' . $field . " LIKE '%" . $keyword . "%'" : $qs . " OR " . $field . " LIKE '%" . $keyword . "%'";
            }
            $qs = $qs == '' ? '' : $qs . ')';
            $where[$qs] = NULL;
            $this->search = $qs;
        }
    }

    public function draw()
    {
        $this->get_filtering();
        $limit = $this->input->post('length', true);
        $start = $this->input->post('start', true);
        //     $paging = $this->get_paging($keyword);

        // $total = $this->db->count_all_results($this->table);

        $this->whereData = (!is_null($this->whereData)) ? $this->whereData : [];
        $where = (!is_null($this->search)) ? array_merge($this->whereData, [$this->search => null]) : $this->whereData;
        if (!is_null($this->whereData)) $this->db->where($where);
        // $this->get_ordering();
        
        $orderField = $this->input->post('order')[0]['column'];
        
        $orderAD = $this->input->post('order')[0]['dir'];
        $orderColumn = $this->input->post('columns')[$orderField]['data'];
        // $this->db->order_by($orderColumn, $orderAD);
        $paging = $this->get_paging($where);
        
        $qData = $this->db
            ->where($where)
            ->order_by($orderColumn, $orderAD)
            ->limit($limit, $start);
        $result = $paging['total'] > 0 ? $qData->get($this->table)->result_array() : [];

        // $query = $qData->get();

        // $data = array();
        // if ($query !== FALSE && $query->num_rows() > 0) {
        //     foreach ($query->result_array() as $row) {
        //         $data[] = $row;
        //     }
        // }

        $a = $this->db->last_query();
        return [
            'draw' => $this->input->post('draw'),
            // 'recordsTotal' => $total,
            // 'recordsFiltered' => $total,
            'recordsTotal' => $paging['total'],
            'recordsFiltered' => $paging['filtered'],
            'data' => $result,
            'qq' => $a,
            'ss' => $this->search,
            'cc' => $where
        ];
    }
    // public function draw()
    // {
    //     $keyword = empty($this->input->post('search')['value'])? null : $this->input->post('search')['value'];
    //     if (!is_null($keyword)) $this->get_filtering($keyword);
    //     $this->get_ordering();
    //     $result = $this->get_result();
    //     $a = $this->db->last_query();
    //     $paging = $this->get_paging($keyword);

    //     return json_encode([
    //         'draw' => $this->input->post('draw'),
    //         'recordsTotal' => $paging['total'],
    //         'recordsFiltered' => $paging['filtered'],
    //         'data' => $result,
    //         'aa' => $a,
    //         'cc' => $this->input->post('start', true)
    //     ]);
    // }

    private function do_join()
    {
        foreach ($this->joins as $join) {
            $this->db->join($join['table'], $join['cond'], $join['type']);
        }
    }

    private function get_paging($where = [])
    {
        if (count($this->joins) > 0) $this->do_join();
        if (!is_null($where)) $this->db->where($where);
        $total = $this->db->where($where)->count_all_results($this->table);

        // if (count($this->joins) > 0) $this->do_join();
        // if (!is_null($where)) $this->db->where($where);

        return [
            'total' => $total,
            'filtered' => $this->db->where($where)->get($this->table)->num_rows()
        ];
    }

    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    private function set_alias($data)
    {
        foreach (explode(',', $data) as $val) {
            if (stripos($val, 'as')) {
                $alias = trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$2', $val));
                $field = trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$1', $val));
                $this->alias[$alias] = $field;
            }
        }
    }

    public function select($fields)
    {
        $this->db->select($fields);
        $this->set_alias($fields);
        return $this;
    }

    public function where($data)
    {
        $this->db->where($data);
        foreach ($data as $field => $value) {
            $this->whereFields[] = $field;
        }
        $this->whereData = $data;
        return $this;
    }

    public function join($table, $cond, $type = '')
    {
        $this->joins[] = ['table' => $table, 'cond' => $cond, 'type' => $type];
        $this->db->join($table, $cond, $type);
        return $this;
    }
}
